import time
from itertools import combinations
import networkx as nx
import graph_tool as gt

from numpy.random import poisson
from graph_tool.topology import shortest_distance
from graph_tool.generation import random_graph
from networkx import dijkstra_path_length


graph_gt = random_graph(500, lambda: poisson(3), directed=False)

graph_nx = nx.Graph()

for edge in graph_gt.edges():
    graph_nx.add_edge(edge.source(), edge.target)

pair_list_gt = combinations(graph_gt.vertices(), 2)
pair_list_nx = combinations(graph_nx.nodes(), 2)


start_nx = time.time()
for u, v in pair_list_nx:
    try:
        d = dijkstra_path_length(graph_nx, u, v)
    except:
        1+1
time_nx = time.time() - start_nx

start_gt = time.time()
d = shortest_distance(graph_gt)
time_gt = time.time() - start_gt


print('time nx:', time_nx)
print('time gt:', time_gt)

