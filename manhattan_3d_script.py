"""simulation_toric_main_3d_manhattan
...
"""

from itertools import product
import re
import os
import numpy as np

from simulation.cores.utils import merge_error_rates_result_files, writing_one_result
from simulation.cores.manhattan_3d import one_expe_manhattan_3d
from simulation.cores.parallel_workers import main_process
from simulation import MACHINE_ID

ALPHA = np.sqrt(np.pi)
# dimension of the toric code
DIM_LIST = [4 + 2 * i for i in range(6)]
# standard deviation of the input shift error
DEV_LIST = [(310 + 10 * i)/1000 for i in range(16)]

SUMVAR = 1*10**3
ARG_LIST = product(DIM_LIST, DEV_LIST, [SUMVAR for _ in range(5)], [ALPHA])

# This prefix has to be unique among the other files present in the folder
FOLDER = 'results/manhattan/'
PREFIX = MACHINE_ID + '_derdeder_manhattan_3d'
VAR_NAMES = 'sigma logical_error_rate trials'
INDEX_NAME = '_dim_'
MAX_CPU = os.cpu_count() - 1


def writing_func(dim, sigma, error_rate, sumvar):
    """writing_func
    including a prefix for the name of the files.
    """
    writing_one_result(FOLDER + PREFIX,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_rate, sumvar))


if __name__ == '__main__':

    main_process(MAX_CPU, one_expe_manhattan_3d, ARG_LIST, writing_func)

    print('Merging data points...')

    RULE = re.compile(PREFIX + r'\S*')
    FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

    for f in FILE_LIST:
        merge_error_rates_result_files([FOLDER + f], FOLDER + f, x_prec=2)

    print('All merged !')
