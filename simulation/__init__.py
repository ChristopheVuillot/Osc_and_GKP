"""Osc_and_GKP
simulating diverse concatenation of GKP with the toric code.
"""
import os
import platform as pltfrm
from sys import platform
from pathlib import Path
import cffi


DIR_PATH = os.path.dirname(os.path.realpath(__file__))
CDEF_STR = """
           typedef struct {
           int uid;
           int vid;
           int weight;
           } Edge;
           int Init();
           int Process(int node_num, int edge_num, Edge *edges);
           int PrintMatching();
           int GetMatching(int * matching);
           int Clean();
           """

MACHINE_ID = ""
with open(DIR_PATH + "/machine_id.txt", 'r') as id_file:
    MACHINE_ID = id_file.readline().rstrip()
if MACHINE_ID == "":
    print('You can define your own id for your computer in machine_id.txt !')
    MACHINE_ID = pltfrm.node()
print('Currently using ' + MACHINE_ID + ' as id.')

if platform == 'linux':
    BLOSSOM_PATH = DIR_PATH + "/blossom5/libblossom.so"
elif platform == 'darwin':
    BLOSSOM_PATH = DIR_PATH + "/blossom5/libblossom.dylib"

FFI = cffi.FFI()
BLOSSOM = FFI.dlopen(BLOSSOM_PATH)
FFI.cdef(CDEF_STR)
