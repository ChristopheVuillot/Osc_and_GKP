"""simulation_dijkstra_2d
The functions used to simulate in 2d with dijkstra distance
computed with weights based on likelihood of error.
"""
import time
from itertools import combinations
import numpy as np
from numpy import pi
import networkx as nx
#import graph_tool as gt
#from graph_tool.topology import shortest_distance, shortest_path
from scipy.stats import norm

from simulation.cores.utils import measurement
from simulation import BLOSSOM, FFI


def int_to_intpair(num, dim):
    """int_to_intpair
    convert int to pair of remainder and quotient
    """
    return (num % dim, num/dim)


def intpair_to_int(pair, dim):
    """intpair_to_int
    convert pair of int to single int
    """
    return pair[0] + dim*pair[1]


def one_expe_dijkstra_2d(dim, sigma, sumvar, alpha):
    """ one_expe(dim, sigma, sumvar):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    """
    # This variable is used to count the number of wrong error correction
    start_time = time.time()
    spec_time = 0.0
    spec2_time = 0.0
    logical_error = 0

    # creates toric code graph
    code_graph = gt.Graph(directed=False)
    code_graph.add_vertex(dim*dim)
    for i in range(dim):
        for j in range(dim):
            v_base = intpair_to_int((i, j), dim)
            v_right = intpair_to_int(((i+1) % dim, j), dim)
            v_up = intpair_to_int((i, (j+1) % dim), dim)
            code_graph.add_edge(code_graph.vertex(v_base),
                                code_graph.vertex(v_right))
            code_graph.add_edge(code_graph.vertex(v_base),
                                code_graph.vertex(v_up))

    edge_weights = code_graph.new_edge_property('double')
    code_graph.ep.edge_weights = edge_weights

    edge_errors = code_graph.new_edge_property('int')
    code_graph.ep.edge_errors = edge_errors

    for _ in range(sumvar):
        # -----------------------------------------------------------------------#
        # inlize the toric code as a graph
        for edge in code_graph.edges():
            shift_error = np.random.normal(0, sigma)
            measure, error = measurement(shift_error, alpha)

            rate = sum([norm.pdf(measure/2 - pi + 2*pi*k, 0, sigma*alpha) for k in range(-3, 4)]) /\
                sum([norm.pdf(measure/2 + pi*k, 0, sigma*alpha) for k in range(-3, 4)])

            if rate > 0.5:
                code_graph.ep.edge_weights[edge] = 0
            else:
                code_graph.ep.edge_weights[edge] = np.log2(1 - rate) - np.log2(rate)

            code_graph.ep.edge_errors[edge] = error

        # -----------------------------------------------------------------------#
        # If product of four neighbouring edges' weights  is -1,
        # thrtice will be regarded as a defect

        syndrome_list = []
        for vertex in code_graph.vertices():
            syndrome = 1
            for edge in vertex.out_edges():
                syndrome *= code_graph.ep.edge_errors[edge]
            if syndrome < 0:
                syndrome_list.append(vertex)

        # -----------------------------------------------------------------------#
        matching_graph = nx.Graph()
        matching_graph.add_nodes_from(syndrome_list)

        tmp_time = time.time()
        all_distances = shortest_distance(code_graph, weights=code_graph.ep.edge_weights)
        spec_time += time.time() - tmp_time

        # ----------  c processing
        node_num = len(syndrome_list)
        edge_num = int(node_num*(node_num - 1)/2)
        edges = FFI.new('Edge[%d]' % (edge_num))
        cmatching = FFI.new('int[%d]' % (2 * node_num))
        # print( 'no of nodes : {0}, no of edges : {1}'.format(node_num,edge_num) )

        e = 0
        for start, end in combinations(syndrome_list, 2):
            # tmp_time = time.time()
            # distance = shortest_distance(code_graph,
            #                              source=code_graph.vertex(start),
            #                              target=code_graph.vertex(end),
            #                              weights=code_graph.ep.edge_weights)
            distance = all_distances[start][end]
            # spec_time += time.time() - tmp_time
            # matching_graph.add_edge(code_graph.vertex_index[start], code_graph.vertex_index[end], weight=500 - distance)
            # '500-distance' transforms into minimum weight matching
            startid = syndrome_list.index(start)
            endid = syndrome_list.index(end)
            wt = int(distance*10000)
            # print('weight of edge[{0}][{1}] = {2}'.format( startid, endid, wt) )
            edges[e].uid = startid
            edges[e].vid = endid
            edges[e].weight = wt
            e += 1

        # print('printing edges before calling blossom')
        # for e in range(edge_num):
        #     print(edges[e].uid, edges[e].vid, edges[e].weight)

        tmp_time = time.time()
        retVal = BLOSSOM.Init()
        retVal = BLOSSOM.Process(node_num, edge_num, edges)
        # print('Init & Process done !')
        # retVal = self.blossom.PrintMatching()
        nMatching = BLOSSOM.GetMatching(cmatching)
        retVal = BLOSSOM.Clean()
        spec2_time += time.time() - tmp_time

        #matching_list = []
        # print('recieved C matching :')
        for i in range(0, nMatching, 2):
            start = syndrome_list[cmatching[i]]
            end = syndrome_list[cmatching[i + 1]]
            tmp_time = time.time()
            _, edge_path = shortest_path(code_graph,
                                         source=start,
                                         target=end,
                                         weights=code_graph.ep.edge_weights)
            spec_time += time.time() - tmp_time
            for edge in edge_path:
                code_graph.ep.edge_errors[edge] *= (-1)
            #matching_list.append((start, end))
            #matching_list[start] = end
            #matching_list[end] = start
            # print( '{0}, {1} '.format(u,v) )

        #----------- end of c processing
        #tmp_time = time.time()
        #matching_list = nx.max_weight_matching(matching_graph)
        #spec2_time += time.time() - tmp_time

        # -----------------------------------------------------------------------#
        # print(matching_list)

        # for start_index, start in enumerate(syndrome_list):
        #     end = syndrome_list[matching_list[start_index]]
        #     # end = code_graph.vertex(end_index)
        #     #syndrome_list.remove(end)
        #     tmp_time = time.time()
        #     _, edge_path = shortest_path(code_graph,
        #                                  source=start,
        #                                  target=end,
        #                                  weights=code_graph.ep.edge_weights)
        #     spec_time += time.time() - tmp_time
        #     for edge in edge_path:
        #         code_graph.ep.edge_errors[edge] *= (-1)

        # -----------------------------------------------------------------------#

        # To k whether we do the right correction, which means no logical error

        check1 = 1
        check2 = 1
        for i in range(dim):
            edge1 = code_graph.edge(intpair_to_int((i, 0), dim),
                                    intpair_to_int((i, 1), dim))
            edge2 = code_graph.edge(intpair_to_int((0, i), dim),
                                    intpair_to_int((1, i), dim))
            check1 *= code_graph.ep.edge_errors[edge1]
            check2 *= code_graph.ep.edge_errors[edge2]
        if check1 < 0 or check2 < 0:
            logical_error += 1

    error_rate = logical_error / sumvar
    print('dimension', dim)
    print('sigma', sigma)
    print('trials =', sumvar)
    print('-------------')
    print('error rate', error_rate)
    end_time = time.time()
    print('total time graph_tool: ', end_time - start_time)
    print('dijkstra time:', spec_time)
    print('matching time:', spec2_time)
    print('-----------------------------------')
    return (dim, sigma, error_rate, sumvar)


def one_expe_dijkstra_2d_nx(dim, sigma, sumvar, alpha):
    """ one_expe(dim, sigma, sumvar):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    """
    start_time = time.time()
    spec_time = 0.0
    spec2_time = 0.0
    logical_error = 0

    G1 = nx.grid_2d_graph(dim, dim, True)

    for _ in range(sumvar):
        # -----------------------------------------------------------------------#
        # inlize the toric code as a graph
        for (u, v) in G1.edges():
            shift_error = np.random.normal(0, sigma)
            measure, error = measurement(shift_error, alpha)

            rate = sum([norm.pdf(measure/2 - np.pi + 2*np.pi*k, 0, sigma*alpha) for k in range(-3, 4)])/\
                   sum([norm.pdf(measure/2 + np.pi*k, 0, sigma*alpha) for k in range(-3, 4)])

            if rate > 0.5:
                G1[u][v]['weight'] = 0
            else:
                G1[u][v]['weight'] = np.log2(1 - rate) - np.log2(rate)

            G1[u][v]['error'] = error

        # -----------------------------------------------------------------------#

        # If product of four neighbouring edges' weights  is -1,
        # thrtice will be regarded as a defect

        syndrome_list = []
        for u in G1.nodes():
            syndrome = 1
            for v in G1[u]:
                syndrome *= G1[u][v]['error']
            if syndrome < 0:
                syndrome_list.append(u)

        # -----------------------------------------------------------------------#

        G3 = nx.Graph()
        G3.add_nodes_from(syndrome_list)
        n = len(syndrome_list)
        for i in range(n):
            for j in range(i+1, n):
                u = syndrome_list[i]
                v = syndrome_list[j]
                tmp_time = time.time()
                w = nx.dijkstra_path_length(G1, u, v, weight='weight')
                spec_time += time.time() - tmp_time
                G3.add_edge(u, v, weight=500 - w)
                # This '50-w' will transform the max-weight matching into minimum weight matching
        tmp_time = time.time()
        matching_list = nx.max_weight_matching(G3)
        spec2_time += time.time() - tmp_time

        # -----------------------------------------------------------------------#

        for u in syndrome_list:
            start = u
            end = matching_list[u]
            syndrome_list.remove(end)
            tmp_time = time.time()
            path = nx.dijkstra_path(G1, start, end, weight='weight')
            spec_time += time.time() - tmp_time

            length = len(path)
            for i in range(length-1):
                u = path[i]
                v = path[i+1]
                G1[u][v]['error'] *= (-1)

        # -----------------------------------------------------------------------#

        # check no syndrome left
        #########################
        # syndrome_list = []
        # for u in G1.nodes():
        #     syndrome = 1
        #     for v in G1[u]:
        #         syndrome *= G1[u][v]['error']
        #     if syndrome < 0:
        #         syndrome_list.append(u)
        # assert len(syndrome_list) == 0
        ######################################

        check1 = 1
        check2 = 1
        for i in range(dim):
            check1 *= G1[(i, 0)][(i, 1)]['error']
            check2 *= G1[(0, i)][(1, i)]['error']
        if check1 < 0 or check2 < 0:
            logical_error += 1

    error_rate = logical_error / sumvar
    print('dimension', dim)
    print('sigma', sigma)
    print('trials =', sumvar)
    print('-------------')
    print('error rate', error_rate)
    end_time = time.time()
    print('total time networkx: ', end_time - start_time)
    print('dijkstra time:', spec_time)
    print('matching time:', spec2_time)
    print('-----------------------------------')
    return (dim, sigma, error_rate, sumvar)
