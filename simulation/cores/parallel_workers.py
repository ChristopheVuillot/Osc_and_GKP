"""parallel_workers
give a simple framework to execute tasks in parallel.
The model is that some workers conduct tasks stored in a queue,
outputing results in another queue. The result queue is accessed
by only one process that is writing results to files.
"""

from multiprocessing import cpu_count, JoinableQueue, Queue, Process

WORKER_STOP_STR = 'STOP'
WRITER_ALL_DONE_STR = 'ALL_DONE'


def queue_worker(input_queue, output_queue):
    """queue_worker
    the function run by the independent processes.
    """
    for func, arguments in iter(input_queue.get, WORKER_STOP_STR):
        output_queue.put(func(*arguments))
        input_queue.task_done()
    print('Nothing left to do !')
    input_queue.task_done()


def writer(writing_func, done_queue):
    """writer
    the function for the unique writer collecting and
    writing results to files
    """
    for args in iter(done_queue.get, WRITER_ALL_DONE_STR):
        writing_func(*args)


def main_process(max_n_cpu, worker_func, arg_list, writing_func):
    """main_process(max_n_cpu, worker_func, arg_list):
    The process that launches all the tasks in parallel.
    """
    n_cpu = min(cpu_count(), max_n_cpu)
    print('Running on '+str(n_cpu)+' processes !')
    job_queue = JoinableQueue()
    results_queue = Queue()

    for args in arg_list:
        job_queue.put((worker_func, args))

    for _ in range(n_cpu):
        job_queue.put(WORKER_STOP_STR)
        Process(target=queue_worker,
                args=(job_queue, results_queue)).start()

    # This writer process has to be started exactly once
    # It operates alone since it is writing to files.
    proc_writer = Process(target=writer, args=(writing_func, results_queue))
    proc_writer.start()

    job_queue.join()
    results_queue.put(WRITER_ALL_DONE_STR)

    print('Everything has been done and written')
