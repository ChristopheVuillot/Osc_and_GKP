"""simulation_one_gkp
...
"""

from itertools import product
import numpy as np
from numpy import pi, exp, sin, cos
from numpy.linalg import multi_dot
from numpy.random import normal
from scipy.optimize import minimize_scalar

from simulation.cores.utils import measurement


def sample_errors(sigma, sigma_meas, alpha, n_steps):
    """sample_errors(sigma, sigma_meas, alpha, n_steps):
    generate displacement errors and measurment outcomes
    for n_steps rounds.
    """
    cumul_shift_errors = np.zeros(n_steps+1)
    meas_outcomes = np.zeros(n_steps+1)

    # initial random displacement on data
    cumul_shift_errors[0] = normal(0, sigma)

    for j in range(n_steps):
        # perform measurement with noise
        meas_outcome, _ = measurement(cumul_shift_errors[j] + normal(0, sigma_meas), alpha)
        meas_outcomes[j] = meas_outcome
        # apply random displacement on data
        cumul_shift_errors[j+1] = cumul_shift_errors[j] + normal(0, sigma)

    # Do one last perfect measurement
    meas_outcome, _ = measurement(cumul_shift_errors[n_steps], alpha)
    meas_outcomes[n_steps] = meas_outcome

    return cumul_shift_errors, meas_outcomes


def batch_of_trials(sigma, sigma_meas, n_steps, alpha, n_trials, decoder):
    """batch_of_trials(sigma, sigma_meas, n_steps, alpha, n_trials, decoder):
    runs n_trials using decoder
    """
    logical_error_no_memory_count = 0

    for _ in range(n_trials):
        # sample errors and measurements
        cumul_shifts, meas_outcomes = sample_errors(sigma, sigma_meas, alpha, n_steps)
        # decode
        final_shift, _ = decoder(sigma, sigma_meas, alpha, n_steps, cumul_shifts, meas_outcomes)
        # check
        check_code_space, logical_error = measurement(cumul_shifts[n_steps] - final_shift, alpha)
        # assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_no_memory_count += 1

    error_rate = logical_error_no_memory_count / n_trials
    return (sigma, sigma_meas, n_steps, error_rate, n_trials)


def one_expe_one_gkp_no_monitoring(sigma, n_steps, alpha, n_trials):
    """ one_expe_one_gkp_no_monitoring(sigma, n_steps, alpha, n_trials):
    compute an estimate of the logical error probability.
    Does n_trials different trial.
    """
    logical_error_no_memory_count = 0

    for _ in range(n_trials):

        # sample errors and measurements
        cumul_shifts, meas_outcomes = sample_errors(sigma, sigma_meas, alpha, n_steps)

        # correct and be back in the code space
        final_shift = cumul_shifts[n_steps] - meas_outcomes[n_steps]/(2*alpha)
        check_code_space, logical_error = measurement(cumul_shift_error, alpha)
        assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_no_memory_count += 1

    error_rate = logical_error_no_memory_count / n_trials
    return (sigma, n_steps, error_rate, n_trials)


def one_expe_one_gkp_no_memory(sigma, sigma_meas, n_steps, alpha, n_trials):
    """ one_expe_one_gkp_no_memory(sigma, sigma_meas, n_steps, alpha, n_trials):
    compute an estimate of the logical error probability.
    Does n_trials different trial.
    """

    logical_error_no_memory_count = 0

    for _ in range(n_trials):

        # initial random displacement on data
        cumul_shift_error = normal(0, sigma)

        for _ in range(n_steps):

            # sample random noise for the measurement
            meas_error = normal(0, sigma_meas)

            # measure
            meas_outcome, _ = measurement(cumul_shift_error + meas_error, alpha)

            # correct immediatly
            cumul_shift_error -= meas_outcome/(2*alpha)

            # apply random displacement on data
            cumul_shift_error += normal(0, sigma)

        # Do one last perfect measurement
        meas_outcome, _ = measurement(cumul_shift_error, alpha)

        # correct and be back in the code space
        cumul_shift_error -= meas_outcome/(2*alpha)
        check_code_space, logical_error = measurement(cumul_shift_error, alpha)
        assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_no_memory_count += 1

    error_rate = logical_error_no_memory_count / n_trials
    return (sigma, sigma_meas, n_steps, error_rate, n_trials)


def one_expe_one_gkp_no_meas_error(sigma, n_steps, alpha, n_trials):
    """ one_expe_one_gkp_no_memory(sigma, n_steps, alpha, n_trials):
    compute an estimate of the logical error probability.
    Does n_trials different trial.
    """

    logical_error_no_memory_count = 0

    for _ in range(n_trials):

        # initial random displacement on data
        cumul_shift_error = normal(0, sigma)

        for _ in range(n_steps):

            # measure
            meas_outcome, _ = measurement(cumul_shift_error, alpha)

            # correct immediatly
            cumul_shift_error -= meas_outcome/(2*alpha)

            # apply random displacement on data
            cumul_shift_error += normal(0, sigma)

        # Do one last perfect measurement
        meas_outcome, _ = measurement(cumul_shift_error, alpha)

        # correct and be back in the code space
        cumul_shift_error -= meas_outcome/(2*alpha)
        check_code_space, logical_error = measurement(cumul_shift_error, alpha)
        assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_no_memory_count += 1

    error_rate = logical_error_no_memory_count / n_trials
    return (sigma, n_steps, error_rate, n_trials)


def iterator_integers(n_steps, cutoff):
    """iterator_integers(n_steps, cutoff):
    returns an iterator over tuples of integers ranging from
    -cutoff to cutoff
    """
    return product(*[range(-cutoff, cutoff+1) for _ in range(n_steps+1)])


def energy(phi_1, phi_0, q_1, sigma, sigma_meas, alpha):
    """energy(phi_1, phi_0, q_1, sigma, sigma_meas, alpha):
    """
    return (phi_1 - phi_0)**2/(2*sigma**2) - cos(q_1 - 2*alpha*phi_1)/(4*alpha**2*sigma_meas**2)


def energy_derivative(phi_1, phi_0, q_1, sigma, sigma_meas, alpha):
    """energy_derivative(phi_1, phi_0, q_1, sigma, sigma_meas, alpha):
    """
    return (phi_1 - phi_0)/sigma**2 - sin(q_1 - 2*alpha*phi_1)/(2*alpha*sigma_meas**2)


def energy_2derivative(phi_1, q_1, sigma, sigma_meas, alpha):
    """energy_2derivative(phi_1, q_1, sigma, sigma_meas, alpha):
    """
    return 1/sigma**2 + cos(q_1 - 2*alpha*phi_1)/sigma_meas**2


def energy_3derivative(phi_1, q_1, sigma_meas, alpha):
    """energy_3derivative(phi_1, q_1, sigma_meas, alpha):
    """
    return 2*alpha*sin(q_1 - 2*alpha*phi_1)/sigma_meas**2


def dyn_prog_min(phi_array, n_steps, sigma, sigma_meas, meas_outcomes, alpha):
    phi_n = len(phi_array)
    mins = np.zeros([n_steps, phi_n], dtype='float')
    for j in range(phi_n):
        mins[0][j] = phi_array[j]**2/(2*sigma**2) - cos(meas_outcomes[0] - 2*alpha*phi_array[j])/(4*alpha**2*sigma_meas**2)
    for k in range(1, n_steps):
        for j in range(phi_n):
            mins[k][j] = min(mins[k-1] + (phi_array - phi_array[j])**2/(2*sigma**2) - cos(meas_outcomes[k] - 2*alpha*phi_array[j])/(4*alpha**2*sigma_meas**2))
    return mins


def dyn_min_energy_decoding(meas_outcomes, sigma, sigma_meas, alpha, n_disc, K):
    n_steps = len(meas_outcomes) - 1
    last_meas = meas_outcomes[n_steps]
    phi_array = np.array([last_meas + j*2*pi/n_disc for j in range(-K*n_disc, K*n_disc+1)])/(2*alpha)
    mins = dyn_prog_min(phi_array, n_steps, sigma, sigma_meas, meas_outcomes, alpha)
    bestj = K
    realmin = min(mins[n_steps-1] + (phi_array - phi_array[bestj*n_disc])**2/(2*sigma**2))
    for j in range(2*K + 1):
        newmin = min(mins[n_steps-1] + (phi_array - phi_array[j*n_disc])**2/(2*sigma**2))
        if newmin < realmin:
            realmin = newmin
            bestj = j
    return (last_meas + (bestj-K)*2*pi)/(2*alpha)


def minimum_energy_decoding_gkp(meas_outcomes, sigma, sigma_meas, alpha):
    """minimum_energy_decoding_gkp(meas_outcomes, sigma, sigma_meas, alpha):
    """
    n_steps = len(meas_outcomes) - 1

    phi = 0.0
    bestphi = 0.0
    for j in range(n_steps):
        old_phi = bestphi
        res_opt = minimize_scalar(energy, args=(old_phi, meas_outcomes[j], sigma, sigma_meas, alpha), bracket=(old_phi-3*pi/alpha, old_phi+3*pi/alpha))
        phi = res_opt.x
        bestphi = phi

    k = np.floor((alpha*bestphi + pi/2)/pi)
    assert meas_outcomes[n_steps] + 2*(k-1)*pi < 2*alpha*bestphi and meas_outcomes[n_steps] + 2*(k+1)*pi > 2*alpha*bestphi

    if meas_outcomes[n_steps] + 2*k*pi < 2*alpha*bestphi:
        if 2*alpha*bestphi - meas_outcomes[n_steps] - 2*k*pi < meas_outcomes[n_steps] + 2*(k+1)*pi - 2*alpha*bestphi:
            final_phi = meas_outcomes[n_steps]/(2*alpha) + k*pi/alpha
        else:
            final_phi = meas_outcomes[n_steps]/(2*alpha) + (k+1)*pi/alpha
    else:
        if 2*alpha*bestphi - meas_outcomes[n_steps] - 2*(k-1)*pi < meas_outcomes[n_steps] + 2*k*pi - 2*alpha*bestphi:
            final_phi = meas_outcomes[n_steps]/(2*alpha) + (k-1)*pi/alpha
        else:
            final_phi = meas_outcomes[n_steps]/(2*alpha) + k*pi/alpha
    return final_phi


def one_expe_one_gkp_minE(sigma, sigma_meas, n_steps, alpha, n_trials):
    """ one_expe_one_gkp_min_energy(sigma, sigma_meas, n_steps, alpha, n_trials):
    compute an estimate of the logical error probability.
    Does n_trials different trial.
    """

    meas_outcomes = np.zeros(n_steps + 1)

    logical_error_count = 0

    for _ in range(n_trials):

        # initial random displacement on data
        cumul_shift_error = normal(0, sigma)

        for step in range(n_steps):

            # sample random noise for the measurement
            meas_error = normal(0, sigma_meas)

            # measure and store the outcome
            meas_outcome, _ = measurement(cumul_shift_error + meas_error, alpha)
            meas_outcomes[step] = meas_outcome

            # apply random displacement on data
            cumul_shift_error += normal(0, sigma)

        # Do one last perfect measurement
        meas_outcome, _ = measurement(cumul_shift_error, alpha)
        meas_outcomes[n_steps] = meas_outcome

        # Do ML decoding
        correction_shift = minimum_energy_decoding_gkp(meas_outcomes, sigma, sigma_meas, alpha)
        # correction_shift = dyn_min_energy_decoding(meas_outcomes, sigma, sigma_meas, alpha, 100, 1)

        # be back in the code space
        check_code_space, logical_error = measurement(cumul_shift_error - correction_shift, alpha)
        assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_count += 1

    error_rate = logical_error_count / n_trials
    return (sigma, sigma_meas, n_steps, error_rate, n_trials)


def next_ints(integers, K, n):
    """next_ints(integers, K, n)
    """
    carry = 1
    k = 0
    while carry == 1 and k < n:
        if integers[k] < K:
            integers[k] += 1
            carry = 0
        else:
            integers[k] = -K
            k = k+1


def maximum_likelihood_decoding_gkp(meas_outcomes, sigma, sigma_meas, alpha, C, C_elem, C_row, K):
    """maximum_likelihood_decoding_gkp(meas_outcomes)
    """
    n_steps = len(meas_outcomes) - 1
    # The last measurement is perfect so it gives us a candidate
    # correction shift to go back to the code space
    correction_shift = meas_outcomes[n_steps]/(2*alpha)

    # Now we find out if it was more likely that a logical error happened
    res_trivial = 0
    res_error = 0
    normalization = 0
    m_vector = -K*np.ones(n_steps+1)

    for _ in range((2*K+1)**(n_steps+1)):
        g_m = meas_outcomes/(2*alpha) + m_vector*pi/alpha
        g_m_prime = g_m[:n_steps]
        g_m_last = g_m[n_steps]
        term = exp(multi_dot([g_m_prime/sigma_meas**2, C/2, g_m_prime/sigma_meas**2]) \
                            + g_m_last**2/sigma**4*C_elem/2 \
                            + multi_dot([g_m_last*g_m_prime/(sigma**2*sigma_meas**2), C_row]))
        normalization += term
        if m_vector[n_steps] % 2 == 0:
            res_trivial += term
        else:
            res_error += term
        next_ints(m_vector, K, n_steps+1)

    # print('res_error(norm) :', res_error, '(', res_error/normalization, ') - res_trivial(norm) :', res_trivial, '(', res_trivial/normalization, ')')
    if res_error > res_trivial:
        correction_shift += pi/alpha
        return correction_shift, res_error/normalization
    return correction_shift, res_trivial/normalization


def one_expe_one_gkp_mldecoder(sigma, sigma_meas, n_steps, n_trials, alpha, K):
    """ one_expe_one_gkp_mldecoder(sigma, sigma_meas, n_steps, alpha, n_trials):
    compute an estimate of the logical error probability.
    Does n_trials different trial.
    """

    meas_outcomes = np.zeros(n_steps + 1)

    logical_error_count = 0

    C = np.linalg.inv(matrix_C(n_steps, sigma, sigma_meas)) - sigma_meas**2*np.eye(n_steps)
    C_elem = C[n_steps-1][n_steps-1] + sigma_meas**2 - sigma**2
    C_row = np.copy(C[n_steps-1])
    C_row[n_steps-1] += sigma_meas**2

    for _ in range(n_trials):

        # initial random displacement on data
        cumul_shift_error = normal(0, sigma)

        for step in range(n_steps):

            # sample random noise for the measurement
            meas_error = normal(0, sigma_meas)

            # measure and store the outcome
            meas_outcome, _ = measurement(cumul_shift_error + meas_error, alpha)
            meas_outcomes[step] = meas_outcome

            # apply random displacement on data
            cumul_shift_error += normal(0, sigma)

        # Do one last perfect measurement
        meas_outcome, _ = measurement(cumul_shift_error, alpha)
        meas_outcomes[n_steps] = meas_outcome

        # Do ML decoding
        correction_shift, _ = maximum_likelihood_decoding_gkp(meas_outcomes, sigma, sigma_meas, alpha, C, C_elem, C_row, K)

        # be back in the code space
        check_code_space, logical_error = measurement(cumul_shift_error - correction_shift, alpha)
        assert abs(check_code_space) < 10**(-10)

        if logical_error < 0:
            logical_error_count += 1

    error_rate = logical_error_count / n_trials
    return (sigma, sigma_meas, n_steps, error_rate, n_trials)
