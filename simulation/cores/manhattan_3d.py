"""simulation_manhattan_3d
The functions used to simulate in 3d with manhattan distance.
"""
from itertools import combinations

import numpy as np
import networkx as nx

from simulation.cores.utils import measurement
from simulation import BLOSSOM, FFI


def manhattan_distance_3d(dim, weight, start, end):
    """ manhattan_distance_3d(dim, weight, start, end):
    returns the manhattan distance between start and end if the weight
    on the edges are all equal to weight in a 3d grid.
    Considers periodic boundary conditions for dim 0 and 1.
    """
    delta_xmin = 50000
    delta_ymin = 50000

    for modif in [-1, 0, 1]:
        delta_x = abs(end[0]-start[0]+modif*dim)
        delta_y = abs(end[1]-start[1]+modif*dim)
        if delta_x < delta_xmin:
            delta_xmin = delta_x
            modif_minx = modif
        if delta_y < delta_ymin:
            delta_ymin = delta_y
            modif_miny = modif

    delta_z = abs(end[2]-start[2])

    return weight*(delta_xmin + delta_ymin + delta_z), modif_minx, modif_miny


def manhattan_toric_path_3d(start, end, dim, modifx, modify):
    """manhattan_toric_path_3d(start, end, dim, modifx, modify):
    return a manhattan path between start and end on a torus
    specified by the directions modifx and modify
    """
    path = [start]
    nsteps_x = abs(end[0]-start[0]+modifx*dim)
    nsteps_y = abs(end[1]-start[1]+modify*dim)
    nsteps_z = abs(end[2]-start[2])

    if modifx == 0:
        if end[0] - start[0] < 0:
            modifx = -1
        else:
            modifx = +1
    if modify == 0:
        if end[1] - start[1] < 0:
            modify = -1
        else:
            modify = +1

    if end[2] - start[2] < 0:
        modifz = -1
    else:
        modifz = +1

    for j in range(nsteps_x):
        new_v = ((path[j][0] + modifx) % dim,
                 path[j][1],
                 path[j][2])
        path.append(new_v)
    for j in range(nsteps_y):
        new_v = (path[nsteps_x+j][0],
                 (path[nsteps_x+j][1] + modify) % dim,
                 path[nsteps_x+j][2])
        path.append(new_v)
    for j in range(nsteps_z):
        new_v = (path[nsteps_x+nsteps_y+j][0],
                 path[nsteps_x+nsteps_y+j][1],
                 (path[nsteps_x+nsteps_y+j][2] + modifz) % dim)
        path.append(new_v)

    return path


def one_expe_manhattan_3d(dim, sigma, sumvar, alpha):
    """ one_expe(dim, sigma, sumvar):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    code_graph = nx.grid_graph([dim, dim, dim], periodic=True)

    for _ in range(sumvar):
        # -----------------------------------------------------------------------#
        # inlize the toric code as a graph
        for (start, end) in code_graph.edges():
            if abs(end[2] - start[2]) >= dim-1:
                code_graph[start][end]['weight'] = 1
                code_graph[start][end]['error'] = 1
            else:
                shift_error = np.random.normal(0, sigma)
                _, error = measurement(shift_error, alpha)
                code_graph[start][end]['actual_shift'] = shift_error
                code_graph[start][end]['weight'] = 1
                code_graph[start][end]['error'] = error

        # -----------------------------------------------------------------------#

        # If product of four neighbouring edges' weights  is -1,
        # thrtice will be regarded as a defect

        syndrome_list = []
        for vertex in code_graph.nodes():
            syndrome = 1
            for neighbour in code_graph[vertex]:
                syndrome *= code_graph[vertex][neighbour]['error']
            if syndrome < 0:
                syndrome_list.append(vertex)

        # ----------  c processing
        node_num = len(syndrome_list)
        edge_num = int(node_num*(node_num - 1)/2)
        edges = FFI.new('Edge[%d]' % (edge_num))
        cmatching = FFI.new('int[%d]' % (2 * node_num))

        edge_count = 0
        for start, end in combinations(syndrome_list, 2):
            distance, _, _ = manhattan_distance_3d(dim, 1, start, end)
            startid = syndrome_list.index(start)
            endid = syndrome_list.index(end)
            weight = int(distance)
            edges[edge_count].uid = startid
            edges[edge_count].vid = endid
            edges[edge_count].weight = weight
            edge_count += 1

        # print('printing edges before calling blossom')
        # for e in range(edge_num):
        #     print(edges[e].uid, edges[e].vid, edges[e].weight)

        _ = BLOSSOM.Init()
        _ = BLOSSOM.Process(node_num, edge_num, edges)
        matching = BLOSSOM.GetMatching(cmatching)
        _ = BLOSSOM.Clean()

        for i in range(0, matching, 2):
            start = syndrome_list[cmatching[i]]
            end = syndrome_list[cmatching[i + 1]]
            _, modx, mody = manhattan_distance_3d(dim, 1, start, end)
            path = manhattan_toric_path_3d(start, end, dim, modx, mody)
            length = len(path)
            for j in range(length-1):
                start = path[j]
                end = path[j+1]
                code_graph[start][end]['error'] *= (-1)
                code_graph[start][end]['actual_shift'] += alpha

        # -----------------------------------------------------------------------#
        # To k whether we do the right correction, which means no logical error

        check1 = 1
        check2 = 1
        for i in range(dim):
            for j in range(dim):
                check1 *= code_graph[(i, 0, j)][(i, 1, j)]['error']
                check2 *= code_graph[(0, i, j)][(1, i, j)]['error']
        if check1 < 0 or check2 < 0:
            logical_error += 1

    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)
