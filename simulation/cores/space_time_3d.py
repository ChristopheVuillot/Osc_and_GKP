"""space_time_3d
...
"""

from itertools import combinations
import numpy as np
import networkx as nx
from numpy import pi, sqrt, sin, cos
from scipy.special import erf
from scipy.stats import norm
# from mpmath import jtheta
from scipy.optimize import minimize_scalar, minimize

from simulation import BLOSSOM, FFI
from simulation.cores.utils import measurement
from simulation.cores.manhattan_3d import manhattan_distance_3d, manhattan_toric_path_3d
from simulation.cores.one_gkp import energy
from simulation.cores.dijkstra_2d import int_to_intpair, intpair_to_int


def edge_to_int(pairpair, dim):
    """edge_to_int(pairpair, dim):
    convert an edge from 3D graph to an int
    """
    ((x1, y1, z1), (x2, y2, z2)) = pairpair
    if z1 == z2:
        if y1 == y2:
            return intpair_to_int((min(x1, x2), 2*y1), dim)
        return intpair_to_int((x1, (2*min(y1, y2)+1)), dim)
    return intpair_to_int((x1, y1), dim)


def int_to_edge(k, dim, z):
    """int_to_edge(k, dim, z):
    convert an int to an edge in 3D graph at height z
    """
    r = k % dim
    q = int(k/dim)
    if q % 2 == 0:
        return ((r, int(q/2), z), (r+1, int(q/2), z))
    return ((r, int(q/2), z), (r, int(q/2)+1, z))


def energy_back(phi_1, phi_2, phi_0, q_1, sigma, sigma_meas, alpha):
    """energy(phi_1, phi_2, phi_0, q_1, sigma, sigma_meas, alpha):
    """
    return (phi_2 - phi_1)**2/(2*sigma**2) + (phi_1 - phi_0)**2/(2*sigma**2) - cos(q_1 - 2*alpha*phi_1)/(4*alpha**2*sigma_meas**2)


def multivar_energy(bphi_1, bphi_0, bq_1, bq_s, bq_anc, sigma, sigma_meas, alpha, n):
    res = 0
    for j in range(2*n*n):
        res += (bphi_1[j] - bphi_0[j])**2/(2*sigma**2) - cos(bq_1[j] - 2*alpha*bphi_1[j])/(4*alpha**2*sigma_meas**2)
    for j in range(n):
        for k in range(n):
            pair1 = edge_to_int(((j, k, 0), (j+1, k, 0)), n)
            pair2 = edge_to_int(((j, k, 0), (j, k+1, 0)), n)
            pair3 = edge_to_int(((j, k, 0), ((j-1)%n, k, 0)), n)
            pair4 = edge_to_int(((j, k, 0), (j, (k-1)%n, 0)), n)
            star_index = intpair_to_int((j,k), n)
            res -= cos(alpha*(bphi_1[pair1]+bphi_1[pair2]-bphi_1[pair3]-bphi_1[pair4]) - bq_s[star_index])/(alpha**2*sigma_meas**2)
            res -= cos(2*alpha*(bphi_1[pair1]+bphi_1[pair2]-bphi_1[pair3]-bphi_1[pair4]) - 2*bq_s[star_index] - bq_anc[star_index])/(4*alpha**2*sigma_meas**2)
    return res


def compute_edge_weight(meas_outcome, sigma, sigma_meas, alpha, cutoff=3):
    a = (1/sigma**2 + 1/sigma_meas**2)/2
    rate0 = sum([norm.pdf(meas_outcome + 2*pi*k1, 0, 2*sigma_meas*alpha/sqrt(1-1/(2*a)))*
                  sum([erf(sqrt(a)/(2*alpha)*(- (meas_outcome+2*k1*pi)/(2*a*sigma_meas**2) + 2*k2*pi + pi))
                      - erf(sqrt(a)/(2*alpha)*(- (meas_outcome+2*k1*pi)/(2*a*sigma_meas**2) + 2*k2*pi - pi))
                      for k2 in range(-cutoff, cutoff+1) if k2 % 2 == 0])
                for k1 in range(-cutoff, cutoff+1)])
    rate1 = sum([norm.pdf(meas_outcome + 2*pi*k1, 0, 2*sigma_meas*alpha/sqrt(1-1/(2*a)))*\
                  sum([erf(sqrt(a)/(2*alpha)*(- (meas_outcome+2*k1*pi)/(2*a*sigma_meas**2) + 2*k2*pi + pi))\
                      - erf(sqrt(a)/(2*alpha)*(- (meas_outcome+2*k1*pi)/(2*a*sigma_meas**2) + 2*k2*pi - pi)) for k2 in range(-cutoff, cutoff+1) if k2 % 2 == 1])\
                  for k1 in range(-cutoff, cutoff+1)])
    rate = rate1/(rate0 + rate1)
    return np.log2(1 - rate) - np.log2(rate)


def fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph):
    """fill_in_3d_space_time_grid:
    """
    for x in range(dim):
        for y in range(dim):
            # horizontal x edges
            shift_error_x = np.random.normal(0, sigma)
            meas_error_x = np.random.normal(0, sigma_meas)
            code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['shift_error'] = shift_error_x
            code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['cumul_shift_error'] = shift_error_x 
            code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['meas_error'] = meas_error_x
            meas_outcome, _ = measurement(shift_error_x + meas_error_x, alpha)
            code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['meas_outcome'] = meas_outcome

            # horizontal y edges
            shift_error_y = np.random.normal(0, sigma)
            meas_error_y = np.random.normal(0, sigma_meas)
            code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['shift_error'] = shift_error_y
            code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['cumul_shift_error'] = shift_error_y
            code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['meas_error'] = meas_error_y
            meas_outcome, _ = measurement(shift_error_y + meas_error_y, alpha)
            code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['meas_outcome'] = meas_outcome

            # vertical z edges
            meas_error_z = np.random.normal(0, sigma_meas)
            code_graph[(x, y, 0)][(x, y, 1)]['meas_error'] = meas_error_z
            # gkp_ancilla_meas_error = np.random.normal(0, sigma_meas)
            # meas_outcome, _ = measurement(meas_error_z + gkp_ancilla_meas_error, alpha)
            # code_graph[(x, y, 0)][(x, y, 1)]['gkp_anc_stab_outcome'] = meas_outcome

    for x in range(dim):
        for y in range(dim):
            # Stabilizer here the outcome is in [-pi,pi] and not [-2pi, 2pi] as in the paper
            stab_outcome, _ = measurement(code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['cumul_shift_error']\
                                          + code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['cumul_shift_error']\
                                          - code_graph[(x, y, 0)][((x-1) % dim, y, 0)]['cumul_shift_error']\
                                          - code_graph[(x, y, 0)][(x, (y-1) % dim, 0)]['cumul_shift_error']\
                                          + code_graph[(x, y, 0)][(x, y, 1)]['meas_error'], alpha/2)
            code_graph.nodes[(x, y, 0)]['stab_outcome'] = stab_outcome

    for z in range(1, dim+1):
        for x in range(dim):
            for y in range(dim):
                # horizontal x edges
                shift_error_x = np.random.normal(0, sigma)
                meas_error_x = np.random.normal(0, sigma_meas)
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['shift_error'] = shift_error_x
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['cumul_shift_error'] = code_graph[(x, y, z-1)][((x+1) % dim, y, z-1)]['cumul_shift_error'] + shift_error_x 
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_error'] = meas_error_x*int(z != dim)
                meas_outcome, _ = measurement(code_graph[(x, y, z)][((x+1) % dim, y, z)]['cumul_shift_error'] + meas_error_x*int(z != dim), alpha)
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome'] = meas_outcome

                # horizontal y edges
                shift_error_y = np.random.normal(0, sigma)
                meas_error_y = np.random.normal(0, sigma_meas)
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['shift_error'] = shift_error_y
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['cumul_shift_error'] = code_graph[(x, y, z-1)][(x, (y+1) % dim, z-1)]['cumul_shift_error'] + shift_error_y
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_error'] = meas_error_y*int(z != dim)
                meas_outcome, _ = measurement(code_graph[(x, y, z)][(x, (y+1) % dim, z)]['cumul_shift_error'] + meas_error_y*int(z != dim), alpha)
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_outcome'] = meas_outcome

                # vertical z edges
                meas_error_z = np.random.normal(0, sigma_meas)
                code_graph[(x, y, z)][(x, y, (z+1) % (dim+1))]['meas_error'] = meas_error_z*int(z != dim)
                # gkp_ancilla_meas_error = np.random.normal(0, sigma_meas)
                # meas_outcome, _ = measurement(meas_error_z*int(z != dim) + gkp_ancilla_meas_error*int(z != dim), alpha)
                # code_graph[(x, y, z)][(x, y, (z+1) % (dim+1))]['gkp_anc_stab_outcome'] = meas_outcome
                # if dim != z:
                #     code_graph[(x, y, z)][(x, y, (z+1) % (dim+1))]['weight'] = compute_edge_weight(meas_outcome, sigma_meas, sigma_meas, alpha, cutoff=2)
                # else:
                #     code_graph[(x, y, z)][(x, y, (z+1) % (dim+1))]['weight'] = 600000

        for x in range(dim):
            for y in range(dim):
                # Stabilizer here the outcome is in [-pi,pi] and not [-2pi, 2pi] as in the paper
                stab_outcome, _ = measurement(code_graph[(x, y, z)][((x+1) % dim, y, z)]['cumul_shift_error']\
                                              + code_graph[(x, y, z)][(x, (y+1) % dim, z)]['cumul_shift_error']\
                                              - code_graph[(x, y, z)][((x-1) % dim, y, z)]['cumul_shift_error']\
                                              - code_graph[(x, y, z)][(x, (y-1) % dim, z)]['cumul_shift_error']\
                                              + code_graph[(x, y, z)][(x, y, (z+1) % (dim+1))]['meas_error'], alpha/2)
                code_graph.nodes[(x, y, z)]['stab_outcome'] = stab_outcome

    return code_graph


def pre_correction_no_mem(dim, sigma, sigma_meas, alpha, code_graph):
    """
    """
    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['pre_guessed_shift'] = code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome']/(2*alpha)
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['pre_guessed_shift'] = code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_outcome']/(2*alpha)
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['post_guessed_shift'] = code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome']/(2*alpha)
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['post_guessed_shift'] = code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_outcome']/(2*alpha)
    return code_graph


def pre_correction_trivial(dim, sigma, sigma_meas, alpha, code_graph):
    """
    """
    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['pre_guessed_shift'] = 0.0
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['pre_guessed_shift'] = 0.0
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['post_guessed_shift'] = 0.0
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['post_guessed_shift'] = 0.0
            # code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['pre_guessed_shift'] = code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['meas_outcome']/(2*alpha)
            # code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift'] = code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['meas_outcome']/(2*alpha)
            # code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['meas_outcome']/(2*alpha)
            # code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['meas_outcome']/(2*alpha)
    return code_graph


def pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph):
    """pre_correction_minE
    """
    for x in range(dim):
        for y in range(dim):
            # # z edges
            # for z in range(dim+1):
            #     res_opt = minimize_scalar(energy,
            #                               args=(0.0,
            #                                     code_graph[(x, y, z)][(x, y, (z+1)%(dim+1))]['gkp_anc_stab_outcome'],
            #                                     sigma,
            #                                     sigma_meas,
            #                                     alpha),
            #                               bracket=(-3*pi/alpha, 3*pi/alpha))
            #     code_graph[(x, y, z)][(x, y, (z+1)%(dim+1))]['pre_guessed_gkp_anc_shift'] = res_opt.x
            #     code_graph[(x, y, z)][(x, y, (z+1)%(dim+1))]['post_guessed_gkp_anc_shift'] = res_opt.x
            # x edges
            old_phi = 0.0
            for z in range(dim):
                res_opt = minimize_scalar(energy,
                                          args=(old_phi,
                                                code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome'],
                                                sigma,
                                                sigma_meas,
                                                alpha),
                                          bracket=(old_phi-3*pi/alpha, old_phi+3*pi/alpha))
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['pre_guessed_shift'] = res_opt.x
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['post_guessed_shift'] = res_opt.x
                old_phi = res_opt.x
            last_phi = code_graph[(x, y, dim-1)][((x+1) % dim, y, dim-1)]['pre_guessed_shift']
            last_meas = code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['meas_outcome']
            k = np.floor((alpha*last_phi + pi/2)/pi)
            assert last_meas + 2*(k-1)*pi < 2*alpha*last_phi and last_meas + 2*(k+1)*pi > 2*alpha*last_phi

            if  last_meas + 2*k*pi < 2*alpha*last_phi:
                if 2*alpha*last_phi - last_meas - 2*k*pi < last_meas + 2*(k+1)*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                else:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
            else:
                if 2*alpha*last_phi - last_meas - 2*(k-1)*pi < last_meas + 2*k*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                else:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha

            # y edges
            old_phi = 0.0
            for z in range(dim):
                res_opt = minimize_scalar(energy,
                                          args=(old_phi,
                                                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_outcome'],
                                                sigma,
                                                sigma_meas,
                                                alpha),
                                          bracket=(old_phi-3*pi/alpha, old_phi+3*pi/alpha))
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['pre_guessed_shift'] = res_opt.x
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['post_guessed_shift'] = res_opt.x
                old_phi = res_opt.x

            last_phi = code_graph[(x, y, dim-1)][(x, (y+1) % dim, dim-1)]['pre_guessed_shift']
            last_meas = code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['meas_outcome']
            k = np.floor((alpha*last_phi + pi/2)/pi)
            assert last_meas + 2*(k-1)*pi < 2*alpha*last_phi and last_meas + 2*(k+1)*pi > 2*alpha*last_phi

            if  last_meas + 2*k*pi < 2*alpha*last_phi:
                if 2*alpha*last_phi - last_meas - 2*k*pi < last_meas + 2*(k+1)*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                else:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
            else:
                if 2*alpha*last_phi - last_meas - 2*(k-1)*pi < last_meas + 2*k*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                else:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha

            # pass backwards
            # x edges
            for z in range(dim-1, 0, -1):
                old_phi = code_graph[(x,y,dim)][((x+1) % dim, y, dim)]['pre_guessed_shift']
                next_phi = code_graph[(x,y,dim-2)][((x+1) % dim, y, dim-2)]['pre_guessed_shift']
                res_opt = minimize_scalar(energy_back,
                                          args=(next_phi,
                                                old_phi,
                                                code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome'],
                                                sigma,
                                                sigma_meas,
                                                alpha),
                                          bracket=((old_phi+next_phi)/2-3*pi/alpha, (old_phi+next_phi)/2+3*pi/alpha))
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['pre_guessed_shift'] = res_opt.x
                code_graph[(x, y, z)][((x+1) % dim, y, z)]['post_guessed_shift'] = res_opt.x
            # y edges
            for z in range(dim-1, 0, -1):
                old_phi = code_graph[(x,y,dim)][(x, (y+1) % dim, dim)]['pre_guessed_shift']
                next_phi = code_graph[(x,y,dim-2)][(x, (y+1) % dim, dim-2)]['pre_guessed_shift']
                res_opt = minimize_scalar(energy_back,
                                          args=(next_phi,
                                                old_phi,
                                                code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome'],
                                                sigma,
                                                sigma_meas,
                                                alpha),
                                          bracket=((old_phi+next_phi)/2-3*pi/alpha, (old_phi+next_phi)/2+3*pi/alpha))
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['pre_guessed_shift'] = res_opt.x
                code_graph[(x, y, z)][(x, (y+1) % dim, z)]['post_guessed_shift'] = res_opt.x
    return code_graph


def defects_direct(dim, sigma, sigma_meas, alpha, code_graph):
    """
    """
    most_uncertain_value = pi/4
    most_uncertain_value_node = (0,0,0)
    number_defects = 0
    syndrome_list = []
    for x in range(dim):
        for y in range(dim):
            code_graph.nodes[(x,y,0)]['defect_value'] = code_graph.nodes[(x, y, 0)]['stab_outcome']\
                                                        - alpha*code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['pre_guessed_shift']\
                                                        - alpha*code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['pre_guessed_shift']\
                                                        + alpha*code_graph[(x, y, 0)][((x-1) % dim, y, 0)]['pre_guessed_shift']\
                                                        + alpha*code_graph[(x, y, 0)][(x, (y-1) % dim, 0)]['pre_guessed_shift']\
                                                        - alpha*code_graph[(x, y, 0)][(x, y, 1)]['pre_guessed_gkp_anc_shift']

            if abs((code_graph.nodes[(x, y, 0)]['defect_value'] + pi) % (2*pi) - pi) < pi/4 \
               or abs((code_graph.nodes[(x, y, 0)]['defect_value'] + pi) % (2*pi) - pi) > 3*pi/4:
                code_graph.nodes[(x,y,0)]['binary_defect_value'] = 0
            else:
                code_graph.nodes[(x,y,0)]['binary_defect_value'] = 1
                number_defects += 1
                syndrome_list.append((x,y,0))

            uncertain_value = min(abs(abs(code_graph.nodes[(x, y, 0)]['defect_value']) - pi/4),
                                  abs(abs(code_graph.nodes[(x, y, 0)]['defect_value']) - 3*pi/4))
            if most_uncertain_value > uncertain_value:
                most_uncertain_value = uncertain_value
                most_uncertain_value_node = (x, y, 0)
            # code_graph.nodes[(x,y,0)]['meas_discord'] = (code_graph.nodes[(x, y, 0)]['stab_outcome']\
            #                                              - code_graph[(x, y, 0)][((x+1) % dim, y, 0)]['meas_outcome']/2\
            #                                              - code_graph[(x, y, 0)][(x, (y+1) % dim, 0)]['meas_outcome']/2\
            #                                              + code_graph[(x, y, 0)][((x-1) % dim, y, 0)]['meas_outcome']/2\
            #                                              + code_graph[(x, y, 0)][(x, (y-1) % dim, 0)]['meas_outcome']/2\
            #                                              - code_graph[(x, y, 0)][(x, y, 1)]['gkp_anc_stab_outcome']/2\
            #                                              + pi/2) % pi - pi/2
            # if abs(largest_meas_discord) < abs(code_graph.nodes[(x,y,0)]['meas_discord']):
            #     largest_meas_discord = code_graph.nodes[(x,y,0)]['meas_discord']
            #     largest_meas_discord_node = (x, y, 0)
            for z in range(1, dim+1):
                code_graph.nodes[(x,y,z)]['defect_value'] = code_graph.nodes[(x,y,z)]['stab_outcome'] - code_graph.nodes[(x,y,z-1)]['stab_outcome']\
                                                            - alpha*(code_graph[(x, y, z)][((x+1) % dim, y, z)]['pre_guessed_shift']\
                                                                     - code_graph[(x, y, z-1)][((x+1) % dim, y, z-1)]['pre_guessed_shift'])\
                                                            - alpha*(code_graph[(x, y, z)][(x, (y+1) % dim, z)]['pre_guessed_shift']\
                                                                     - code_graph[(x, y, z-1)][(x, (y+1) % dim, z-1)]['pre_guessed_shift'])\
                                                            + alpha*(code_graph[(x, y, z)][((x-1) % dim, y, z)]['pre_guessed_shift']\
                                                                     - code_graph[(x, y, z-1)][((x-1) % dim, y, z-1)]['pre_guessed_shift'])\
                                                            + alpha*(code_graph[(x, y, z)][(x, (y-1) % dim, z)]['pre_guessed_shift']\
                                                                     - code_graph[(x, y, z-1)][(x, (y-1) % dim, z-1)]['pre_guessed_shift'])\
                                                            - alpha*(code_graph[(x, y, z)][(x, y, (z+1)%(dim+1))]['pre_guessed_gkp_anc_shift']\
                                                                     - code_graph[(x, y, (z-2)%(dim+1))][(x, y, (z-1)%(dim+1))]['pre_guessed_gkp_anc_shift'])
                if abs((code_graph.nodes[(x, y, z)]['defect_value'] + pi) % (2*pi) - pi) < pi/4 \
                   or abs((code_graph.nodes[(x, y, z)]['defect_value'] + pi) % (2*pi) - pi) > 3*pi/4:
                    code_graph.nodes[(x,y,z)]['binary_defect_value'] = 0
                else:
                    code_graph.nodes[(x,y,z)]['binary_defect_value'] = 1
                    number_defects += 1
                    syndrome_list.append((x,y,z))
                # code_graph.nodes[(x,y,z)]['meas_discord'] = (code_graph.nodes[(x,y,z)]['stab_outcome'] - code_graph.nodes[(x,y,z-1)]['stab_outcome']
                #                                              - (code_graph[(x, y, z)][((x+1) % dim, y, z)]['meas_outcome']\
                #                                                 - code_graph[(x, y, z-1)][((x+1) % dim, y, z-1)]['meas_outcome'])/2\
                #                                              - (code_graph[(x, y, z)][(x, (y+1) % dim, z)]['meas_outcome']\
                #                                                 - code_graph[(x, y, z-1)][(x, (y+1) % dim, z-1)]['meas_outcome'])/2\
                #                                              + (code_graph[(x, y, z)][((x-1) % dim, y, z)]['meas_outcome']\
                #                                                 - code_graph[(x, y, z-1)][((x-1) % dim, y, z-1)]['meas_outcome'])/2\
                #                                              + (code_graph[(x, y, z)][(x, (y-1) % dim, z)]['meas_outcome']\
                #                                                 - code_graph[(x, y, z-1)][(x, (y-1) % dim, z-1)]['meas_outcome'])/2\
                #                                              - (code_graph[(x, y, z)][(x, y, (z+1)%(dim+1))]['gkp_anc_stab_outcome']\
                #                                                 - code_graph[(x, y, (z-2)%(dim+1))][(x, y, (z-1)%(dim+1))]['gkp_anc_stab_outcome'])/2\
                #                                              + pi/2) % pi - pi/2
                # if abs(largest_meas_discord) < abs(code_graph.nodes[(x,y,z)]['meas_discord']):
                #     largest_meas_discord = code_graph.nodes[(x,y,z)]['meas_discord']
                #     largest_meas_discord_node = (x, y, z)
                uncertain_value = min(abs(abs(code_graph.nodes[(x, y, z)]['defect_value']) - pi/4),
                                      abs(abs(code_graph.nodes[(x, y, z)]['defect_value']) - 3*pi/4))
                if most_uncertain_value > uncertain_value:
                    most_uncertain_value = uncertain_value
                    most_uncertain_value_node = (x, y, z)
    # print('most uncertain value:', most_uncertain_value)
    if number_defects % 2 == 1:
        # print('odd syndrome:', number_defects)
        code_graph.nodes[most_uncertain_value_node]['binary_defect_value'] = 1 - code_graph.nodes[most_uncertain_value_node]['binary_defect_value']
        syndrome_list = set(syndrome_list) ^ {most_uncertain_value_node}
    # else:
        # print('even syndrome:', number_defects)
    # print('adjusted syndrome number:', len(syndrome_list))
    return list(syndrome_list)


def edge_dict_to_array(edge_dict, edge_array, dim):
    for key, val in edge_dict.items():
        edge_array[edge_to_int(key, dim)] = val
    return edge_array


def array_to_edge_dict(edge_dict, edge_array, dim, z):
    for j, val in enumerate(edge_array):
        edge_dict[int_to_edge(j, dim, z)] = val
    return edge_dict


def node_dict_to_array(node_dict, node_array, dim):
    for key, val in node_dict.items():
        node_array[intpair_to_int(key, dim)] = val
    return node_array


def array_to_node_dict(node_dict, node_array, dim):
    for j, val in enumerate(node_array):
        node_dict[int_to_intpair(j, dim)] = val
    return node_dict


def decode_toric_manhattan_last_round(syndrome_list, code_graph, dim, alpha):
    # ----------  c processing
    node_num = len(syndrome_list)
    edge_num = int(node_num*(node_num - 1)/2)
    edges = FFI.new('Edge[%d]' % (edge_num))
    cmatching = FFI.new('int[%d]' % (2 * node_num))

    edge_count = 0
    for start, end in combinations(syndrome_list, 2):
        distance, _, _ = manhattan_distance_3d(dim, 1, start, end)
        startid = syndrome_list.index(start)
        endid = syndrome_list.index(end)
        weight = int(distance)
        edges[edge_count].uid = startid
        edges[edge_count].vid = endid
        edges[edge_count].weight = weight
        edge_count += 1

    _ = BLOSSOM.Init()
    _ = BLOSSOM.Process(node_num, edge_num, edges)
    matching = BLOSSOM.GetMatching(cmatching)
    _ = BLOSSOM.Clean()

    for i in range(0, matching, 2):
        start = syndrome_list[cmatching[i]]
        end = syndrome_list[cmatching[i + 1]]
        _, modx, mody = manhattan_distance_3d(dim, 1, start, end)
        path = manhattan_toric_path_3d(start, end, dim, modx, mody)
        length = len(path)
        for j in range(length-1):
            start = path[j]
            end = path[j+1]
            code_graph[start][end]['post_guessed_shift'] += alpha


def final_syndrome(dim, code_graph, alpha):
    syndrome_list = []
    for x in range(dim):
        for y in range(dim):
            s_val = code_graph.nodes[(x, y, dim)]['stab_outcome']\
                    - alpha*code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift']\
                    - alpha*code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift']\
                    + alpha*code_graph[(x, y, dim)][((x-1) % dim, y, dim)]['post_guessed_shift']\
                    + alpha*code_graph[(x, y, dim)][(x, (y-1) % dim, dim)]['post_guessed_shift']
            if abs((s_val+ pi) % (2*pi) - pi) > pi/2:
                syndrome_list.append((x,y,dim))
    return syndrome_list


def decode_optimize(dim, sigma, sigma_meas, alpha, code_graph):
    """pre_correction_minE
    """
    stab_outcomes_dict = nx.get_node_attributes(code_graph, 'stab_outcome')
    pre_guessed_shift_dict = nx.get_edge_attributes(code_graph, 'pre_guessed_shift')
    gkp_stab_dict = nx.get_edge_attributes(code_graph, 'meas_outcome')
    gkp_anc_stab_dict = nx.get_edge_attributes(code_graph, 'gkp_anc_stab_outcome')
    post_guessed_shift_dict = dict.copy(pre_guessed_shift_dict)
    bphi = np.zeros(2*dim*dim, dtype='double')
    bqj = np.zeros(2*dim*dim, dtype='double')
    bqs = np.zeros(dim*dim, dtype='double')
    bqanc = np.zeros(dim*dim, dtype='double')
    bphi_init = np.zeros(2*dim*dim, dtype='double')
    for z in range(dim):
        edge_dict_to_array({k:v for k,v in pre_guessed_shift_dict.items() if k[0][2] == k[1][2] == z}, bphi_init, dim)
        edge_dict_to_array({k:v for k,v in gkp_stab_dict.items() if k[0][2] == k[1][2] == z}, bqj, dim)
        node_dict_to_array({k:v for k,v in stab_outcomes_dict.items() if k[2]==z}, bqs, dim)
        edge_dict_to_array({k:v for k,v in gkp_anc_stab_dict.items() if k[0][2] == z and k[1][2] == (z+1) % (dim+1)}, bqanc, dim)
        res_opt = minimize(multivar_energy,
                           bphi_init,
                           args=(bphi,
                                 bqj,
                                 bqs,
                                 bqanc,
                                 sigma,
                                 sigma_meas,
                                 alpha,
                                 dim))
        nx.set_edge_attributes(code_graph, array_to_edge_dict(post_guessed_shift_dict, res_opt.x, dim, z),'post_guessed_shift')
        bphi = res_opt.x
    # last round
    for x in range(dim):
        for y in range(dim):
            last_phi = code_graph[(x, y, dim-1)][(x, (y+1) % dim, dim-1)]['post_guessed_shift']
            last_meas = code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['meas_outcome']
            k = np.floor((alpha*last_phi + pi/2)/pi)
            assert last_meas + 2*(k-1)*pi < 2*alpha*last_phi and last_meas + 2*(k+1)*pi > 2*alpha*last_phi

            if  last_meas + 2*k*pi < 2*alpha*last_phi:
                if 2*alpha*last_phi - last_meas - 2*k*pi < last_meas + 2*(k+1)*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                else:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
            else:
                if 2*alpha*last_phi - last_meas - 2*(k-1)*pi < last_meas + 2*k*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                else:
                    code_graph[(x, y, dim)][(x, (y+1) % dim, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
            last_phi = code_graph[(x, y, dim-1)][((x+1) % dim, y, dim-1)]['post_guessed_shift']
            last_meas = code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['meas_outcome']
            k = np.floor((alpha*last_phi + pi/2)/pi)
            assert last_meas + 2*(k-1)*pi < 2*alpha*last_phi and last_meas + 2*(k+1)*pi > 2*alpha*last_phi

            if  last_meas + 2*k*pi < 2*alpha*last_phi:
                if 2*alpha*last_phi - last_meas - 2*k*pi < last_meas + 2*(k+1)*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha
                else:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k+1)*pi/alpha
            else:
                if 2*alpha*last_phi - last_meas - 2*(k-1)*pi < last_meas + 2*k*pi - 2*alpha*last_phi:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + (k-1)*pi/alpha
                else:
                    code_graph[(x, y, dim)][((x+1) % dim, y, dim)]['post_guessed_shift'] = last_meas/(2*alpha) + k*pi/alpha

    syndrome_list = final_syndrome(dim, code_graph, alpha)
    # print(syndrome_list)
    # for e in code_graph.edges():
    #     if e[0][2]==e[1][2]==dim:
    #         print('edge:',e,'post_guess:',code_graph[e[0]][e[1]]['post_guessed_shift'] - code_graph[e[0]][e[1]]['cumul_shift_error'])
    # print('decoding...')
    decode_toric_manhattan_last_round(syndrome_list, code_graph, dim, alpha)
    # for e in code_graph.edges():
    #     if e[0][2]==e[1][2]==dim:
    #         print('edge:',e,'post_guess:',code_graph[e[0]][e[1]]['post_guessed_shift'] - code_graph[e[0]][e[1]]['cumul_shift_error'])
    return code_graph


def logical_error_check(dim, alpha, code_graph):
    check1 = 0
    check2 = 0
    for i in range(dim):
        val = 2*alpha*(code_graph[(i, 0, dim)][(i, 1, dim)]['cumul_shift_error'] - code_graph[(i, 0, dim)][(i, 1, dim)]['post_guessed_shift'])
        err = int(abs((val+2*pi) % (4*pi) - 2*pi) > pi)
        check1 = check1 + err
        val = 2*alpha*(code_graph[(0, i, dim)][(1, i, dim)]['cumul_shift_error'] - code_graph[(0, i, dim)][(1, i, dim)]['post_guessed_shift'])
        err = int(abs((val+2*pi) % (4*pi) - 2*pi) > pi)
        check2 = check2 + err
    # # print('check1 and check2', check1, check2)
    # for j in range(1, dim):
    #     checktmp1 = 0
    #     checktmp2 = 0
    #     for i in range(dim):
    #         val = 2*alpha*(code_graph[(i, j, dim)][(i, (j+1) % dim, dim)]['cumul_shift_error'] - code_graph[(i, j, dim)][(i, (j+1) % dim, dim)]['post_guessed_shift'])
    #         err = int(abs((val+2*pi) % (4*pi) - 2*pi) > pi)
    #         checktmp1 = checktmp1 + err
    #         val = 2*alpha*(code_graph[(j, i, dim)][((j+1) % dim, i, dim)]['cumul_shift_error'] - code_graph[(j, i, dim)][((j+1) % dim, i, dim)]['post_guessed_shift'])
    #         err = int(abs((val+2*pi) % (4*pi) - 2*pi) > pi)
    #         checktmp2 = checktmp2 + err
    #     # print('checktmp1 and checktmp2', checktmp1, checktmp2)
    #     assert (check1 % 2  == checktmp1 % 2) and (check2 % 2 == checktmp2 % 2)
    if check1 % 2 == 1 or check2 % 2 == 1:
        return True
    return False


def decode_dijkstra_nx(dim, alpha, code_graph, syndrome_list):
    node_num = len(syndrome_list)
    edge_num = int(node_num*(node_num - 1)/2)
    edges = FFI.new('Edge[%d]' % (edge_num))
    cmatching = FFI.new('int[%d]' % (2 * node_num))

    edge_count = 0
    for start, end in combinations(syndrome_list, 2):
        distance = nx.dijkstra_path_length(code_graph, start, end, weight='weight')
        startid = syndrome_list.index(start)
        endid = syndrome_list.index(end)
        weight = int(distance*100000)
        edges[edge_count].uid = startid
        edges[edge_count].vid = endid
        edges[edge_count].weight = weight
        edge_count += 1

    # print('printing edges before calling blossom')
    # for e in range(edge_num):
    #     print(edges[e].uid, edges[e].vid, edges[e].weight)

    _ = BLOSSOM.Init()
    _ = BLOSSOM.Process(node_num, edge_num, edges)
    matching = BLOSSOM.GetMatching(cmatching)
    _ = BLOSSOM.Clean()

    for i in range(0, matching, 2):
        start = syndrome_list[cmatching[i]]
        end = syndrome_list[cmatching[i + 1]]
        path = nx.dijkstra_path(code_graph, start, end, weight='weight')
        length = len(path)
        for j in range(length-1):
            (xs, ys, zs) = path[j]
            (xe, ye, ze) = path[j+1]
            if zs == ze:
                code_graph[(xs, ys, dim)][(xe, ye, dim)]['post_guessed_shift'] += pi/alpha
    return code_graph


def decode(dim, alpha, code_graph, syndrome_list):
    node_num = len(syndrome_list)
    edge_num = int(node_num*(node_num - 1)/2)
    edges = FFI.new('Edge[%d]' % (edge_num))
    cmatching = FFI.new('int[%d]' % (2 * node_num))

    edge_count = 0
    for start, end in combinations(syndrome_list, 2):
        distance, _, _ = manhattan_distance_3d(dim, 1, start, end)
        startid = syndrome_list.index(start)
        endid = syndrome_list.index(end)
        weight = int(distance)
        edges[edge_count].uid = startid
        edges[edge_count].vid = endid
        edges[edge_count].weight = weight
        edge_count += 1

    # print('printing edges before calling blossom')
    # for e in range(edge_num):
    #     print(edges[e].uid, edges[e].vid, edges[e].weight)

    _ = BLOSSOM.Init()
    _ = BLOSSOM.Process(node_num, edge_num, edges)
    matching = BLOSSOM.GetMatching(cmatching)
    _ = BLOSSOM.Clean()

    for i in range(0, matching, 2):
        start = syndrome_list[cmatching[i]]
        end = syndrome_list[cmatching[i + 1]]
        _, modx, mody = manhattan_distance_3d(dim, 1, start, end)
        path = manhattan_toric_path_3d(start, end, dim, modx, mody)
        length = len(path)
        for j in range(length-1):
            (xs, ys, zs) = path[j]
            (xe, ye, ze) = path[j+1]
            if zs == ze:
                code_graph[(xs, ys, dim)][(xe, ye, dim)]['post_guessed_shift'] += pi/alpha
    return code_graph


def one_expe_direct_impl(dim, sigma, sigma_meas, sumvar, alpha):
    """ one_expe_direct_impl(dim, sigma, sumvar, alpha):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    uses no_memory decoder for every single gkp and manhattan distance for
    toric code MWPM with adjusting to even number of defects by adding most
    probable one.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    # There is a weird bug in grid_graph, one needs to give the dimensions
    # in reverse order !!
    code_graph = nx.grid_graph([dim+1, dim, dim], periodic=True)

    for _ in range(sumvar):

        fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph)
        pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph)
        syndrome_list = defects_direct(dim, sigma, sigma_meas, alpha, code_graph)

        decode(dim, alpha, code_graph, syndrome_list)
        final_syndrome_list = final_syndrome(dim, code_graph, alpha)
        decode_toric_manhattan_last_round(final_syndrome_list, code_graph, dim, alpha)

        try:
            if logical_error_check(dim, alpha, code_graph):
                logical_error += 1
        except AssertionError:
            print('Not in code spce !!')
            return code_graph

    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)


def one_expe_dijkstra(dim, sigma, sigma_meas, sumvar, alpha):
    """ one_expe_direct_impl(dim, sigma, sumvar, alpha):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    uses no_memory decoder for every single gkp and manhattan distance for
    toric code MWPM with adjusting to even number of defects by adding most
    probable one.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    # There is a weird bug in grid_graph, one needs to give the dimensions
    # in reverse order !!
    code_graph = nx.grid_graph([dim+1, dim, dim], periodic=True)

    for _ in range(sumvar):

        fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph)
        pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph)
        syndrome_list = defects_direct(dim, sigma, sigma_meas, alpha, code_graph)

        decode_dijkstra_nx(dim, alpha, code_graph, syndrome_list)
        final_syndrome_list = final_syndrome(dim, code_graph, alpha)
        decode_toric_manhattan_last_round(final_syndrome_list, code_graph, dim, alpha)

        try:
            if logical_error_check(dim, alpha, code_graph):
                logical_error += 1
        except AssertionError:
            print('Not in code spce !!')
            return code_graph

    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)


def one_expe_minE_impl(dim, sigma, sigma_meas, sumvar, alpha):
    """ one_expe_direct_impl(dim, sigma, sumvar, alpha):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    uses no_memory decoder for every single gkp and manhattan distance for
    toric code MWPM with adjusting to even number of defects by adding most
    probable one.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    # There is a weird bug in grid_graph, one needs to give the dimensions
    # in reverse order !!
    code_graph = nx.grid_graph([dim+1, dim, dim], periodic=True)

    for _ in range(sumvar):

        fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph)
        pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph)
        syndrome_list = defects_direct(dim, sigma, sigma_meas, alpha, code_graph)

        decode(dim, alpha, code_graph, syndrome_list)

        if logical_error_check(dim, alpha, code_graph):
            logical_error += 1

    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)


def one_expe_optimize(dim, sigma, sigma_meas, sumvar, alpha):
    """ one_expe_optimize(dim, sigma, sumvar, alpha):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    uses no_memory decoder for every single gkp and manhattan distance for
    toric code MWPM with adjusting to even number of defects by adding most
    probable one.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    # There is a weird bug in grid_graph, one needs to give the dimensions
    # in reverse order !!
    code_graph = nx.grid_graph([dim+1, dim, dim], periodic=True)

    for _ in range(sumvar):

        fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph)
        pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph)
        decode_optimize(dim, sigma, sigma_meas, alpha, code_graph)
        try:
            if logical_error_check(dim, alpha, code_graph):
                logical_error += 1
        except AssertionError:
            print('Not in code spce !!')
            return code_graph
    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)


def one_expe_no_monotoring_impl(dim, sigma, sigma_meas, sumvar, alpha):
    """ one_expe_no_monitoring_impl(dim, sigma, sigma_meas, sumvar, alpha):
    compute an estimate of the logical error probability for lattice size dim
    and spread of the displacement errors sigma. Does sumvar different trial.
    uses no_memory decoder for every single gkp and manhattan distance for
    toric code MWPM with adjusting to even number of defects by adding most
    probable one.
    """
    logical_error = 0
    # This variable is used to count the number of wrong error correction

    # There is a weird bug in grid_graph, one needs to give the dimensions
    # in reverse order !!
    code_graph = nx.grid_graph([dim+1, dim, dim], periodic=True)

    for _ in range(sumvar):

        fill_in_3d_space_time_grid(dim, sigma, sigma_meas, alpha, code_graph)
        pre_correction_minE(dim, sigma, sigma_meas, alpha, code_graph)
        syndrome_list = final_syndrome(dim, code_graph, alpha)
        decode_toric_manhattan_last_round(syndrome_list, code_graph, dim, alpha)

        if logical_error_check(dim, alpha, code_graph):
            logical_error += 1

    error_rate = logical_error / sumvar
    return (dim, sigma, error_rate, sumvar)
