"""dual_space_time_3d
...
"""

from itertools import product
import numpy as np
from numpy import pi, sqrt, sin, cos, sign
from numpy.random import normal, rand
# import gurobipy as grb
import networkx as nx
from scipy.optimize import minimize_scalar

from simulation.cores.utils import measurement, print_array
from simulation.cores.one_gkp import energy

from simulation import BLOSSOM, FFI


def print_edges(array, dim):
    print(print_array(array, lambda j: int_to_edge(j, dim)))


def print_vertices(array, dim):
    print(print_array(array, lambda j: int_to_triple(j, dim)))


def triple_to_int(x, y, z, dim):
    """
    convert from three coordinates vertex to an integer such that
    j = x + y*dim + z*dim^2
    """
    xmod = x % dim
    ymod = y % dim
    return xmod + dim*ymod + dim*dim*z


def int_to_triple(j, dim):
    """
    convert from integer to a three coordinate vertex
    j = x + y*dim + z*dim^2
    """
    z = int(j/(dim*dim))
    jxy = j % (dim*dim)
    y = int(jxy/dim)
    x = jxy % dim
    return (x, y, z)


def edge_to_int(vertex1, vertex2, dim):
    """
    convert from an edge to an int such that
    jedge = mu + jvertex*3
    where mu is the edge direction and jvertex the smallest
    of the two vertices converted to an int
    """
    x1, y1, z1 = vertex1
    x1 = x1 % dim
    y1 = y1 % dim
    x2, y2, z2 = vertex2
    x2 = x2 % dim
    y2 = y2 % dim
    if (x1 != x2) & (y1 == y2) & (z1 == z2):
        x = min(x1, x2)
        if abs(x1 - x2) == dim-1:
            x = dim-1
        j = triple_to_int(x, y1, z1, dim)
        return j*3
    if (x1 == x2) & (y1 != y2) & (z1 == z2):
        y = min(y1, y2)
        if abs(y1 - y2) == dim-1:
            y = dim-1
        j = triple_to_int(x1, y, z1, dim)
        return 1 + j*3
    if (x1 == x2) & (y1 == y2) & (z1 != z2):
        z = min(z1, z2)
        j = triple_to_int(x1, y2, z, dim)
        return 2 + j*3
    return -1


def int_to_edge(j, dim):
    """
    convert from an int to an edge such that
    jedge = mu + jvertex*3
    where mu is the edge direction and jvertex the smallest
    of the two vertices converted to an int
    """
    jxyz = int(j/3)
    x, y, z = int_to_triple(jxyz, dim)
    mu = j % 3
    if mu == 0:
        return ((x, y, z), ((x+1) % dim, y, z))
    if mu == 1:
        return ((x, y, z), (x, (y+1) % dim, z))
    if mu == 2:
        return ((x, y, z), (x, y, z+1))


def apply_pure_error(val, tup, dim, data_errors, only_last_round=False):
    """
    adds the pure error corresponding to syndrome val at position tup
    """
    x, y, z = tup
    if only_last_round:
        for j in range(1, x+1):
            j_edge = edge_to_int((j-1, 0, z), (j, 0, z), dim)
            data_errors[j_edge] -= val
        for j in range(1, y+1):
            j_edge = edge_to_int((x, j-1, z), (x, j, z), dim)
            data_errors[j_edge] -= val
    else:
        for j in range(1, x+1):
            j_edge = edge_to_int((j-1, 0, 0), (j, 0, 0), dim)
            data_errors[j_edge] -= val
        for j in range(1, y+1):
            j_edge = edge_to_int((x, j-1, 0), (x, j, 0), dim)
            data_errors[j_edge] -= val
        for j in range(1, z+1):
            j_edge = edge_to_int((x, y, j-1), (x, y, j), dim)
            data_errors[j_edge] -= val
    return


def energy_backward(phi_1, phi_0, phi_2, q_1, sigma, sigma_meas, alpha):
    return (phi_1 - phi_0)**2/(2*sigma**2) \
           + (phi_1 - phi_2)**2/(2*sigma**2) \
           - cos(q_1 - 2*alpha*phi_1)/(4*alpha**2*sigma_meas**2)


def one_gkp_minE_fwdbwd(v1, v2, cumul_data, syndrome_gkp, sigma, sigma_meas, dim, alpha):
    x1, y1 = v1
    x2, y2 = v2
    prev_phi = 0.0
    for z in range(dim):
        jedge = edge_to_int((x1, y1, z), (x2, y2, z), dim)
        res_opt = minimize_scalar(energy,
                                  args=(prev_phi,
                                        syndrome_gkp[jedge],
                                        sigma,
                                        sigma_meas,
                                        alpha),
                                  bracket=(prev_phi-3*pi/(2*alpha), prev_phi+3*pi/(2*alpha)))
        cumul_data[jedge] = res_opt.x
        prev_phi = res_opt.x

    k = np.floor((alpha*prev_phi + pi/2)/pi)
    jlastedge = edge_to_int((x1, y1, dim), (x2, y2, dim), dim)
    # assert syndrome_gkp[jlastedge] + 2*(k-1)*pi < 2*alpha*prev_phi and syndrome_gkp[jlastedge] + 2*(k+1)*pi > 2*alpha*prev_phi

    if syndrome_gkp[jlastedge] + 2*k*pi < 2*alpha*prev_phi:
        if 2*alpha*prev_phi - syndrome_gkp[jlastedge] - 2*k*pi < syndrome_gkp[jlastedge] + 2*(k+1)*pi - 2*alpha*prev_phi:
            cumul_data[jlastedge] = syndrome_gkp[jlastedge]/(2*alpha) + k*pi/alpha
        else:
            cumul_data[jlastedge] = syndrome_gkp[jlastedge]/(2*alpha) + (k+1)*pi/alpha
    else:
        if 2*alpha*prev_phi - syndrome_gkp[jlastedge] - 2*(k-1)*pi < syndrome_gkp[jlastedge] + 2*k*pi - 2*alpha*prev_phi:
            cumul_data[jlastedge] = syndrome_gkp[jlastedge]/(2*alpha) + (k-1)*pi/alpha
        else:
            cumul_data[jlastedge] = syndrome_gkp[jlastedge]/(2*alpha) + k*pi/alpha

    for z in reversed(range(1,dim)):
        jprevedge = edge_to_int((x1, y1, z-1), (x2, y2, z-1), dim)
        jedge = edge_to_int((x1, y1, z), (x2, y2, z), dim)
        jnextedge = edge_to_int((x1, y1, z+1), (x2, y2, z+1), dim)
        prev_phi = cumul_data[jprevedge]
        next_phi = cumul_data[jnextedge]
        res_opt = minimize_scalar(energy_backward,
                                  args=(prev_phi,
                                        next_phi,
                                        syndrome_gkp[jedge],
                                        sigma,
                                        sigma_meas,
                                        alpha),
                                  bracket=(min(prev_phi, next_phi)-pi/(2*alpha), max(prev_phi, next_phi)+pi/(2*alpha)))
        cumul_data[jedge] = res_opt.x
    jedge = edge_to_int((x1, y1, 0), (x2, y2, 0), dim)
    jnextedge = edge_to_int((x1, y1, 1), (x2, y2, 1), dim)
    prev_phi = 0.0
    next_phi = cumul_data[jnextedge]
    res_opt = minimize_scalar(energy_backward,
                              args=(prev_phi,
                                    next_phi,
                                    syndrome_gkp[jedge],
                                    sigma,
                                    sigma_meas,
                                    alpha),
                              bracket=(min(prev_phi, next_phi)-pi/alpha, max(prev_phi, next_phi)+pi/alpha))
    cumul_data[jedge] = res_opt.x


def gkp_pre_correction(syndrome_gkp, sigma, sigma_meas, dim, alpha):
    """
    returns decoding for individual gkp records
    """
    data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    cumul_data = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    meas_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')

    for x in range(dim):
        for y in range(dim):
            one_gkp_minE_fwdbwd((x, y), (x+1, y), cumul_data, syndrome_gkp, sigma, sigma_meas, dim, alpha)
            one_gkp_minE_fwdbwd((x, y), (x, y+1), cumul_data, syndrome_gkp, sigma, sigma_meas, dim, alpha)

    for x in range(dim):
        for y in range(dim):
            jx = edge_to_int((x, y, 0), (x+1, y, 0), dim)
            jy = edge_to_int((x, y, 0), (x, y+1, 0), dim)
            data_errors[jx] = cumul_data[jx]
            data_errors[jy] = cumul_data[jy]
            for z in range(1, dim+1):
                jx_prev = edge_to_int((x, y, z-1), (x+1, y, z-1), dim)
                jy_prev = edge_to_int((x, y, z-1), (x, y+1, z-1), dim)
                jx = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy = edge_to_int((x, y, z), (x, y+1, z), dim)
                data_errors[jx] =  cumul_data[jx] - cumul_data[jx_prev]
                data_errors[jy] = cumul_data[jy] - cumul_data[jy_prev]

    for j, syn in enumerate(syndrome_gkp):
        meas_errors[j] = syn/(2*alpha) - cumul_data[j]

    return data_errors, meas_errors, cumul_data


def clever_error_candidate(syndrome_toric, syndrome_gkp, sigma, sigma_meas, dim, alpha):
    data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    cumul_data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    meas_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')

    pre_data_errors, pre_meas_errors, pre_cumul_data_errors = gkp_pre_correction(syndrome_gkp, sigma, sigma_meas, dim, alpha)
    mod_syn_toric, mod_syn_gkp = measure_syndrome(pre_data_errors, pre_meas_errors, pre_cumul_data_errors, dim, alpha)
    data_errors, meas_errors, cumul_data_errors = error_candidate(syndrome_toric - mod_syn_toric, syndrome_gkp - mod_syn_gkp, dim, alpha)

    return data_errors, pre_data_errors, meas_errors, pre_meas_errors, cumul_data_errors, pre_cumul_data_errors


def error_candidate(syndrome_toric, syndrome_gkp, dim, alpha):
    """
    returns a candidate error given the syndrome
    """
    data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    cumul_data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    meas_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')

    index_syndrome = enumerate(syndrome_toric)
    next(index_syndrome)
    # this above skips the first toric syndrome at (0,0,0)
    # which is redundant with the others
    for j, syn in index_syndrome:
        tup = int_to_triple(j, dim)
        x, y, z = tup
        apply_pure_error(syn/(2*alpha), tup, dim, data_errors)

    for x in range(dim):
        for y in range(dim):
            jx = edge_to_int((x, y, 0), (x+1, y, 0), dim)
            jy = edge_to_int((x, y, 0), (x, y+1, 0), dim)
            cumul_data_errors[jx] = data_errors[jx]
            cumul_data_errors[jy] = data_errors[jy]
            for z in range(1, dim+1):
                jx_prev = edge_to_int((x, y, z-1), (x+1, y, z-1), dim)
                jy_prev = edge_to_int((x, y, z-1), (x, y+1, z-1), dim)
                jx = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy = edge_to_int((x, y, z), (x, y+1, z), dim)
                if z == dim:
                    # Last cumul gkp measure is perfect
                    cumul_data_errors[jx] = syndrome_gkp[jx]/(2*alpha)
                    cumul_data_errors[jy] = syndrome_gkp[jy]/(2*alpha)
                    data_errors[jx] = cumul_data_errors[jx] - cumul_data_errors[jx_prev]
                    data_errors[jy] = cumul_data_errors[jy] - cumul_data_errors[jy_prev]
                else:
                    cumul_data_errors[jx] = cumul_data_errors[jx_prev] + data_errors[jx]
                    cumul_data_errors[jy] = cumul_data_errors[jy_prev] + data_errors[jy]

    for j, syn in enumerate(syndrome_gkp):
        ((x1, y1, z1), (x2, y2, z2)) = int_to_edge(j, dim)
        meas_errors[j] = syn/(2*alpha) - cumul_data_errors[j]

    # The constraint on last round having perfect gkp measurement
    # can leave some logical gkp errors
    candi_syn_toric, _ = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
    for x in range(dim):
        for y in range(dim):
            j = triple_to_int(x, y, dim, dim)
            syn = candi_syn_toric[j]-syndrome_toric[j]
            apply_pure_error(syn/(2*alpha), (x, y, dim), dim, data_errors, only_last_round=True)

    return data_errors, meas_errors, compute_cumul(data_errors, dim)


def smallest_shift_to_2pi(syn):
    if syn < 0:
        if syn + 2*pi < -syn:
            shift = syn + 2*pi
        else:
            shift = syn
    else:
        if 2*pi - syn < syn:
            shift = syn - 2*pi
        else:
            shift = syn
    return shift


def adjust_vertical_toric_errors(syn_toric, data_errors, dim, alpha):
    for x in range(dim):
        for y in range(dim):
            syn = syn_toric[triple_to_int(x, y, 0, dim)]
            shift = smallest_shift_to_2pi(syn)/(2*alpha)
            data_errors[edge_to_int((x, y, 0), (x, y, 1), dim)] = shift
            for z in range(1, dim):
                syn = syn_toric[triple_to_int(x, y, z, dim)]
                shift = smallest_shift_to_2pi(syn)/(2*alpha)
                jz = edge_to_int((x, y, z), (x, y, z+1), dim)
                data_errors[jz] = shift + data_errors[jz - 3*dim**2]


def error_candidate_no_gkp_err(syndrome_toric, syndrome_gkp, dim, alpha):
    """
    returns a candidate error given the syndrome
    """
    data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    cumul_data_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')
    meas_errors = np.zeros(3*(dim + 1)*dim**2, dtype='double')

    for j, syn in enumerate(syndrome_gkp):
        ((x1, y1, z1), (x2, y2, z2)) = int_to_edge(j, dim)
        cumul_data_errors[j] = syn/(2*alpha)

    for x in range(dim):
        for y in range(dim):
            jx = edge_to_int((x, y, 0), (x+1, y, 0), dim)
            jy = edge_to_int((x, y, 0), (x, y+1, 0), dim)
            data_errors[jx] = cumul_data_errors[jx]
            data_errors[jy] = cumul_data_errors[jy]
            for z in range(1, dim+1):
                jx_prev = edge_to_int((x, y, z-1), (x+1, y, z-1), dim)
                jy_prev = edge_to_int((x, y, z-1), (x, y+1, z-1), dim)
                jx = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy = edge_to_int((x, y, z), (x, y+1, z), dim)
                data_errors[jx] = cumul_data_errors[jx] - cumul_data_errors[jx_prev]
                data_errors[jy] = cumul_data_errors[jy] - cumul_data_errors[jy_prev]

    # can leave some logical gkp errors
    candi_syn_toric, _ = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
    # print((syndrome_toric - candi_syn_toric))

    # adjust_vertical_toric_errors(candi_syn_toric, data_errors, dim, alpha)
    # candi_syn_toric, _ = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)

    index_syndrome = enumerate(syndrome_toric)
    next(index_syndrome)
    # this above skips the first toric syndrome at (0,0,0)
    # which is redundant with the others
    for j, syn in index_syndrome:
        tup = int_to_triple(j, dim)
        x, y, z = tup
        apply_pure_error((syn - candi_syn_toric[j])/(2*alpha), tup, dim, data_errors)

    return data_errors, meas_errors, compute_cumul(data_errors, dim)


def measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha):
    """
    compute the syndrome in terms of data and meas errors
    the convention is that gkp syndromes are in [-pi, pi]
    and toric code syndromes are in [-2pi, 2pi].
    The syndrome computed is that of the difference charges for toric checks
    and the standard cummulative ones for the gkp checks.
    WARNING: doesn't work with dim=2 because edge (0,0,0)->(1,0,0)
    should be different from edge (1,0,0)->(0,0,0) in that case but
    it is not for our encoding of edges.
    """
    syndrome_toric = np.zeros((dim + 1)*dim**2, dtype='double')
    syndrome_gkp = np.zeros(3*(dim + 1)*dim**2, dtype='double')

    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                jtoric = triple_to_int(x, y, z, dim)
                jgkpx_plus = edge_to_int((x, y, z), (x+1, y, z), dim)
                jgkpy_plus = edge_to_int((x, y, z), (x, y+1, z), dim)
                jgkpz_plus = edge_to_int((x, y, z), (x, y, z+1), dim)
                jgkpx_minus = edge_to_int((x, y, z), (x-1, y, z), dim)
                jgkpy_minus = edge_to_int((x, y, z), (x, y-1, z), dim)
                jgkpz_minus = edge_to_int((x, y, z), (x, y, z-1), dim)

                outcome, _ = measurement(cumul_data_errors[jgkpx_plus] + meas_errors[jgkpx_plus], alpha)
                syndrome_gkp[jgkpx_plus] = outcome
                outcome, _ = measurement(cumul_data_errors[jgkpy_plus] + meas_errors[jgkpy_plus], alpha)
                syndrome_gkp[jgkpy_plus] = outcome

                if z == 0:
                    outcome, _ = measurement(data_errors[jgkpx_plus] \
                                            + data_errors[jgkpy_plus] \
                                            - data_errors[jgkpx_minus] \
                                            - data_errors[jgkpy_minus] \
                                            + data_errors[jgkpz_plus], alpha/2)
                    syndrome_toric[jtoric] = 2*outcome
                elif z < dim:
                    outcome, _ = measurement(data_errors[jgkpx_plus] \
                                            + data_errors[jgkpy_plus] \
                                            - data_errors[jgkpx_minus] \
                                            - data_errors[jgkpy_minus] \
                                            + data_errors[jgkpz_plus] \
                                            - data_errors[jgkpz_minus], alpha/2)
                    syndrome_toric[jtoric] = 2*outcome
                    # if syndrome_toric[jtoric] != 0:
                    #     print('2alpha({} + {} - {} - {} + {} - {}) = {} mod 4pi'.format(data_errors[jgkpx_plus],
                    #                                                                     data_errors[jgkpy_plus],
                    #                                                                     data_errors[jgkpx_minus],
                    #                                                                     data_errors[jgkpy_minus],
                    #                                                                     data_errors[jgkpz_plus],
                    #                                                                     data_errors[jgkpz_minus],
                    #                                                                     2*outcome))
                elif z == dim:
                    outcome, _ = measurement(data_errors[jgkpx_plus] \
                                            + data_errors[jgkpy_plus] \
                                            - data_errors[jgkpx_minus] \
                                            - data_errors[jgkpy_minus] \
                                            - data_errors[jgkpz_minus], alpha/2)
                    syndrome_toric[jtoric] = 2*outcome
    return syndrome_toric, syndrome_gkp


def get_sigma_list(sigma_data_err, sigma_toric_meas_err, dim):
    sigma_list = np.zeros(3*(dim+1)*dim**2, dtype='double')
    for j in range(3*(dim+1)*dim**2):
        if (j % 3) == 2:
            sigma_list[j] = sigma_toric_meas_err
        else:
            sigma_list[j] = sigma_data_err
    return sigma_list


def compute_cumul(data_errors, dim):
    cumul_data_errors = np.zeros(3*(dim+1)*dim**2, dtype='double')
    for x in range(dim):
        for y in range(dim):
            jx = edge_to_int((x, y, 0), (x+1, y, 0), dim)
            jy = edge_to_int((x, y, 0), (x, y+1, 0), dim)
            cumul_data_errors[jx] = data_errors[jx]
            cumul_data_errors[jy] = data_errors[jy]
            for z in range(1, dim+1):
                jx_prev = edge_to_int((x, y, z-1), (x+1, y, z-1), dim)
                jy_prev = edge_to_int((x, y, z-1), (x, y+1, z-1), dim)
                jx = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy = edge_to_int((x, y, z), (x, y+1, z), dim)
                cumul_data_errors[jx] = cumul_data_errors[jx_prev] + data_errors[jx]
                cumul_data_errors[jy] = cumul_data_errors[jy_prev] + data_errors[jy]
    return cumul_data_errors


def sample_error(sigma_list, sigma_meas, dim):
    """
    """
    data_errors = normal(0.0, sigma_list)
    meas_errors = normal(0.0, sigma_meas, 3*(dim+1)*dim**2)
    for x in range(dim):
        for y in range(dim):
            jx_plus = edge_to_int((x, y, dim), (x+1, y, dim), dim)
            jy_plus = edge_to_int((x, y, dim), (x, y+1, dim), dim)
            jz_plus = edge_to_int((x, y, dim), (x, y, dim+1), dim)
            meas_errors[jx_plus] = 0.0
            meas_errors[jy_plus] = 0.0
            data_errors[jz_plus] = 0.0
            for z in range(dim+1):
                jz_plus = edge_to_int((x, y, z), (x, y, z+1), dim)
                meas_errors[jz_plus] = 0.0
    cumul_data_errors = compute_cumul(data_errors, dim)
    return data_errors, meas_errors, cumul_data_errors


def sample_discrete_error(p, dim, alpha):
    """
    """
    data_errors = pi/alpha*(rand(3*(dim+1)*dim**2) < p)
    meas_errors = np.zeros(3*(dim+1)*dim**2)
    for x in range(dim):
        for y in range(dim):
            jz_plus = edge_to_int((x, y, dim), (x, y, dim+1), dim)
            data_errors[jz_plus] = 0.0
    cumul_data_errors = compute_cumul(data_errors, dim)
    return data_errors, meas_errors, cumul_data_errors


def sample_discrete_error_3level(p, dim, alpha):
    """
    """
    data_errors = rand(3*(dim+1)*dim**2)
    for j in range(3*(dim+1)*dim**2):
        if data_errors[j] < p:
            data_errors[j] = pi/(2*alpha)
        elif data_errors[j] < 3*p/2:
            data_errors[j] = pi/alpha
        else:
            data_errors[j] = 0.0
    meas_errors = np.zeros(3*(dim+1)*dim**2)
    for x in range(dim):
        for y in range(dim):
            jz_plus = edge_to_int((x, y, dim), (x, y, dim+1), dim)
            data_errors[jz_plus] = 0.0
    cumul_data_errors = compute_cumul(data_errors, dim)
    return data_errors, meas_errors, cumul_data_errors


def generate_nx_graph(candidate_data, candidate_meas, sigma_list, dim, alpha):
    list_edges = []
    node_dict = dict.fromkeys(range((dim+1)*dim**2), 1)
    for j, eps in enumerate(candidate_data):
        v1, v2 = int_to_edge(j, dim)
        j1 = triple_to_int(v1[0], v1[1], v1[2], dim)
        j2 = triple_to_int(v2[0], v2[1], v2[2], dim)
        if v1[2] == v2[2]:
            jprev = edge_to_int((v1[0], v1[1], v1[2]-1), (v2[0], v2[1], v2[2]-1), dim)
            if jprev >= 0:
                deltadelta = -candidate_meas[j] + candidate_meas[jprev]
            else:
                deltadelta = -candidate_meas[j]
        else:
            jxplus = edge_to_int(v1, (v1[0]+1, v1[1], v1[2]), dim)
            jxminus = edge_to_int(v1, (v1[0]-1, v1[1], v1[2]), dim)
            jyplus = edge_to_int(v1, (v1[0], v1[1]+1, v1[2]), dim)
            jyminus = edge_to_int(v1, (v1[0], v1[1]-1, v1[2]), dim)
            deltadelta = candidate_meas[jxplus] - candidate_meas[jxminus] + candidate_meas[jyplus] - candidate_meas[jyminus]
        edge_sign = sign(cos(alpha*eps - alpha*deltadelta))
        if j1 < (dim+1)*dim**2:
            node_dict[j1] *= edge_sign
        if j2 < (dim+1)*dim**2:
            node_dict[j2] *= edge_sign
        edge_weight = abs(1/(alpha**2*sigma_list[j]**2)*cos(alpha*eps - alpha*deltadelta))
        # print(edge_weight)
        list_edges.append("{} {} {{\'weight\':{}, \'sign\':{}}}".format(j1, j2, edge_weight, edge_sign))
    code_graph = nx.parse_edgelist(list_edges, nodetype=int)
    nx.set_node_attributes(code_graph, node_dict, name='defect')
    defect_list = [k for k, s in node_dict.items() if s < 0]
    return code_graph, defect_list


def generate_nx_graph_no_deltadelta(candidate_data, sigma_list, dim, alpha):
    list_edges = []
    node_dict = dict.fromkeys(range((dim+1)*dim**2), 1)
    for j, eps in enumerate(candidate_data):
        v1, v2 = int_to_edge(j, dim)
        j1 = triple_to_int(v1[0], v1[1], v1[2], dim)
        j2 = triple_to_int(v2[0], v2[1], v2[2], dim)
        edge_sign = sign(cos(alpha*eps))
        if j1 < (dim+1)*dim**2:
            node_dict[j1] *= edge_sign
        if j2 < (dim+1)*dim**2:
            node_dict[j2] *= edge_sign
        edge_weight = abs(1/(alpha**2*sigma_list[j]**2)*cos(alpha*eps))
        # print(edge_weight)
        list_edges.append("{} {} {{\'weight\':{}, \'sign\':{}}}".format(j1, j2, edge_weight, edge_sign))
    code_graph = nx.parse_edgelist(list_edges, nodetype=int)
    nx.set_node_attributes(code_graph, node_dict, name='defect')
    defect_list = [k for k, s in node_dict.items() if s < 0]
    return code_graph, defect_list


def generate_nx_matching_graph(code_graph, defect_list):
    matching_graph = nx.complete_graph(defect_list)
    distance = dict(nx.all_pairs_dijkstra_path_length(code_graph))
    for e in matching_graph.edges():
        matching_graph.edges[e]['weight'] = distance[e[0]][e[1]]
    return matching_graph


def min_weight_matching(code_graph, matching_graph):
    # ----------  c processing
    node_list = list(matching_graph.nodes())
    node_num = len(node_list)
    edge_num = int(node_num*(node_num - 1)/2)
    edges = FFI.new('Edge[%d]' % (edge_num))
    cmatching = FFI.new('int[%d]' % (2 * node_num))
    # print( 'no of nodes : {0}, no of edges : {1}'.format(node_num,edge_num) )

    e = 0
    for edge in matching_graph.edges():
        vertex1, vertex2 = edge
        distance = matching_graph[vertex1][vertex2]['weight']
        startid = node_list.index(vertex1)
        endid = node_list.index(vertex2)
        wt = int(distance*1000)
        # print('weight of edge[{0}][{1}] = {2}'.format( startid, endid, wt) )
        edges[e].uid = startid
        edges[e].vid = endid
        edges[e].weight = wt
        e += 1

    # print('printing edges before calling blossom')
    # for e in range(edge_num):
    #     print(edges[e].uid, edges[e].vid, edges[e].weight)

    _ = BLOSSOM.Init()
    _ = BLOSSOM.Process(node_num, edge_num, edges)
    # print('Init & Process done !')
    # retVal = self.blossom.PrintMatching()
    nMatching = BLOSSOM.GetMatching(cmatching)
    _ = BLOSSOM.Clean()

    # matching_list = []
    # print('recieved C matching :')
    for i in range(0, nMatching, 2):
        start = node_list[cmatching[i]]
        end = node_list[cmatching[i + 1]]
        node_path = nx.dijkstra_path(code_graph,
                                     start,
                                     end)
        node_prev = node_path[0]
        for j in range(1, len(node_path)):
            code_graph[node_prev][node_path[j]]['sign'] *= (-1)
            node_prev = node_path[j]
        # matching_list.append((start, end))
        # matching_list[start] = end
        # matching_list[end] = start
        # print( '{0}, {1} '.format(u,v) )

    # ----------- end of c processing


def correction(code_graph, candidate_data, candidate_meas, candidate_cumul, dim, alpha):
    correction_data = np.copy(candidate_data)
    correction_meas = np.copy(candidate_meas)

    for j in range(3*(dim+1)*dim**2):
        vertex1, vertex2 = int_to_edge(j, dim)
        j1 = triple_to_int(vertex1[0], vertex1[1], vertex1[2], dim)
        j2 = triple_to_int(vertex2[0], vertex2[1], vertex2[2], dim)
        if code_graph[j1][j2]['sign'] < 0:
            correction_data[j] += pi/alpha
    correction_cumul = compute_cumul(correction_data, dim)
    return correction_data, correction_meas, correction_cumul


def decode_min_gkp(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    candidate_data_toric, candidate_date_gkp, candidate_meas_toric, candidate_meas_gkp, candidate_cumul_toric, candidate_cumul_gkp = clever_error_candidate(syn_toric, syn_gkp, sigma_list[0], sigma_meas, dim, alpha)
    code_graph, defect_list = generate_nx_graph_no_deltadelta(candidate_data_toric+candidate_date_gkp, sigma_list, dim, alpha)
    matching_graph = generate_nx_matching_graph(code_graph, defect_list)
    min_weight_matching(code_graph, matching_graph)
    return correction(code_graph, candidate_data_toric + candidate_date_gkp, candidate_meas_toric + candidate_meas_gkp, candidate_cumul_toric + candidate_cumul_gkp, dim, alpha)


def decode_direct(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    candidate_data, candidate_meas, candidate_cumul = error_candidate(syn_toric, syn_gkp, dim, alpha)
    code_graph, defect_list = generate_nx_graph(candidate_data, candidate_meas, sigma_list, dim, alpha)
    matching_graph = generate_nx_matching_graph(code_graph, defect_list)
    min_weight_matching(code_graph, matching_graph)
    return correction(code_graph, candidate_data, candidate_meas, candidate_cumul, dim, alpha)


def decode_candi(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    return error_candidate(syn_toric, syn_gkp, dim, alpha)


def decode_only_min_gkp(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    return clever_error_candidate(syn_toric, syn_gkp, sigma_list[0], sigma_meas, dim, alpha)


def decode_no_gkp_err(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    candidate_data, candidate_meas, candidate_cumul = error_candidate_no_gkp_err(syn_toric, syn_gkp, dim, alpha)
    code_graph, defect_list = generate_nx_graph(candidate_data, candidate_meas, sigma_list, dim, alpha)
    matching_graph = generate_nx_matching_graph(code_graph, defect_list)
    min_weight_matching(code_graph, matching_graph)
    return correction(code_graph, candidate_data, candidate_meas, candidate_cumul, dim, alpha)


def decode_no_gkp_err_direct(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha):
    return error_candidate_no_gkp_err(syn_toric, syn_gkp, dim, alpha)


def check_logical_error(cumul_data_errors, correction_cumul, dim, alpha):
    countx = 0
    county = 0
    for mu in range(dim):
        jx = edge_to_int((0, mu, dim), (1, mu, dim), dim)
        jy = edge_to_int((mu, 0, dim), (mu, 1, dim), dim)
        countx += np.floor((cumul_data_errors[jx] - correction_cumul[jx] + pi/(3*alpha))/(pi/alpha))
        county += np.floor((cumul_data_errors[jy] - correction_cumul[jy] + pi/(2*alpha))/(pi/alpha))
    return ((countx % 2) == 1) or ((county % 2) == 1)


def batch_of_trials(sigma_list, sigma_meas, dim, alpha, n_trials, decoder):
    """batch_of_trials(sigma, sigma_meas, n_steps, alpha, n_trials, decoder):
    runs n_trials using decoder
    """
    logical_error_count = 0

    for _ in range(n_trials):
        # sample errors and measurements
        data_errors, meas_errors, cumul_data_errors = sample_error(sigma_list, sigma_meas, dim)
        # compute observed syndrome
        syn_toric, syn_gkp = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
        # decode
        correction_data, correction_meas, correction_cumul = decoder(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha)
        # check
        # res_syn_toric, res_syn_gkp = measure_syndrome(data_errors - correction_data, meas_errors - correction_meas, cumul_data_errors - correction_cumul, dim, alpha)
        # assert np.allclose(res_syn_toric, 0)
        # assert np.allclose(res_syn_gkp, 0)
        logical_error_count += check_logical_error(cumul_data_errors, correction_cumul, dim, alpha)

    return (sigma_list[0], sigma_list[2], sigma_meas, dim, logical_error_count, n_trials)


def batch_of_trials_discrete(p, sigma_list, sigma_meas, dim, alpha, n_trials, decoder):
    """batch_of_trials(sigma, sigma_meas, n_steps, alpha, n_trials, decoder):
    runs n_trials using decoder
    """
    logical_error_count = 0

    for _ in range(n_trials):
        # sample errors and measurements
        data_errors, meas_errors, cumul_data_errors = sample_discrete_error(p, dim, alpha)
        # compute observed syndrome
        syn_toric, syn_gkp = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
        # decode
        correction_data, correction_meas, correction_cumul = decoder(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha)
        # check
        # res_syn_toric, res_syn_gkp = measure_syndrome(data_errors - correction_data, meas_errors - correction_meas, cumul_data_errors - correction_cumul, dim, alpha)
        # assert np.allclose(res_syn_toric, 0)
        # assert np.allclose(res_syn_gkp, 0)
        logical_error_count += check_logical_error(cumul_data_errors, correction_cumul, dim, alpha)

    return (p, dim, logical_error_count, n_trials)


def batch_of_trials_discrete_3level(p, sigma_list, sigma_meas, dim, alpha, n_trials, decoder):
    """batch_of_trials(sigma, sigma_meas, n_steps, alpha, n_trials, decoder):
    runs n_trials using decoder
    """
    logical_error_count = 0

    for _ in range(n_trials):
        # sample errors and measurements
        data_errors, meas_errors, cumul_data_errors = sample_discrete_error_3level(p, dim, alpha)
        # compute observed syndrome
        syn_toric, syn_gkp = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
        # decode
        correction_data, correction_meas, correction_cumul = decoder(syn_toric, syn_gkp, sigma_list, sigma_meas, dim, alpha)
        # check
        # res_syn_toric, res_syn_gkp = measure_syndrome(data_errors - correction_data, meas_errors - correction_meas, cumul_data_errors - correction_cumul, dim, alpha)
        # assert np.allclose(res_syn_toric, 0)
        # assert np.allclose(res_syn_gkp, 0)
        logical_error_count += check_logical_error(cumul_data_errors, correction_cumul, dim, alpha)

    return (p, dim, logical_error_count, n_trials)


# Gurobi quadratic optimization stuffs, probably not useful in the end
# ####################################################################
def divergence(variables, dim):
    res = [0 for j in range((dim+1)*dim**2)]
    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                jx_plus = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy_plus = edge_to_int((x, y, z), (x, y+1, z), dim)
                jx_minus = edge_to_int((x, y, z), (x-1, y, z), dim)
                jy_minus = edge_to_int((x, y, z), (x, y-1, z), dim)
                res[triple_to_int(x, y, z, dim)] += variables[jx_plus] + variables[jy_plus] \
                                                    - variables[jx_minus] - variables[jy_minus]
                
                jz_plus = edge_to_int((x, y, z), (x, y, z+1), dim)
                jz_minus = edge_to_int((x, y, z), (x, y, z-1), dim)
                if z > 0:
                    res[triple_to_int(x, y, z, dim)] -= variables[jz_minus]
                if z < dim:
                    res[triple_to_int(x, y, z, dim)] += variables[jz_plus]
    return res


def rotational(variables, dim):
    """
    compute the rotational of the variables field
    the variables fields lives on the edges of the dual lattice
    the res field on the edges of the primal lattice.
    variables anf res are indexed starting from opposite corners.
    """
    res = [0 for j in range(3*(dim+1)*dim**2)]
    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                #x plaquette
                jx_0 = edge_to_int((x, y, z), (x, y+1, z), dim)
                jx_1 = edge_to_int((x, y+1, z), (x, y+1, z+1), dim)
                jx_2 = edge_to_int((x, y+1, z+1), (x, y, z+1), dim)
                jx_3 = edge_to_int((x, y, z+1), (x, y, z), dim)
                #y plaquette
                jy_0 = edge_to_int((x, y, z), (x+1, y, z), dim)
                jy_1 = edge_to_int((x+1, y, z), (x+1, y, z+1), dim)
                jy_2 = edge_to_int((x+1, y, z+1), (x, y, z+1), dim)
                jy_3 = edge_to_int((x, y, z+1), (x, y, z), dim)
                #z plaquette
                jz_0 = edge_to_int((x, y, z), (x+1, y, z), dim)
                jz_1 = edge_to_int((x+1, y, z), (x+1, y+1, z), dim)
                jz_2 = edge_to_int((x+1, y+1, z), (x, y+1, z), dim)
                jz_3 = edge_to_int((x, y+1, z), (x, y, z), dim)

                if z < dim:
                    res[edge_to_int((dim-1-x, dim-1-y, dim-z), (dim-x, dim-1-y, dim-z), dim)] += variables[jx_2] + variables[jx_1] \
                                                                                            - variables[jx_0] - variables[jx_3]
                else:
                    res[edge_to_int((dim-1-x, dim-1-y, dim-z), (dim-x, dim-1-y, dim-z), dim)] += variables[jx_1] \
                                                                                            - variables[jx_0] - variables[jx_3]

                if z < dim:
                    res[edge_to_int((dim-1-x, dim-1-y, dim-z), (dim-1-x, dim-y, dim-z), dim)] += variables[jy_2] + variables[jy_1] \
                                                                                             - variables[jy_0] - variables[jy_3]
                else:
                    res[edge_to_int((dim-1-x, dim-1-y, dim-z), (dim-1-x, dim-y, dim-z), dim)] += variables[jy_1] \
                                                                                             - variables[jy_0] - variables[jy_3]
                res[edge_to_int((dim-1-x, dim-1-y, dim-z), (dim-1-x, dim-1-y, dim-z+1), dim)] += variables[jz_2] + variables[jz_1] \
                                                                                                - variables[jz_0] - variables[jz_3]
    return res


def objective_B(variables, candidate_errors, sigma_list, dim, alpha):
    """
    For now it is not a valid PSD objective function for gurobi,
    need to put all the cos to +1 by factoring out logical gkp errors ?
    """
    added_errors = np.zeros(3*(dim+1)*dim**2)
    obj_B = 0
    for j in range(3*(dim+1)*dim**2):
        if cos(alpha*candidate_errors[j]) < 0:
            added_errors[j] = 1
            obj_B += 4/sigma_list[j]*( \
                     - cos(alpha*candidate_errors[j])*(1 - alpha*variables[j]*alpha*variables[j]/2) \
                     + sin(alpha*candidate_errors[j])*alpha*variables[j])
        else:
            obj_B += 4/sigma_list[j]*( \
                     + cos(alpha*candidate_errors[j])*(1 - alpha*variables[j]*alpha*variables[j]/2) \
                     - sin(alpha*candidate_errors[j])*alpha*variables[j])
    return obj_B, added_errors


def objective_AB(A, B, candidate_errors, candidate_meas, sigma_list, sigma_meas, dim, alpha):
    """
    For now it is not a valid PSD objective function for gurobi,
    need to put all the cos to +1 by factoring out logical gkp errors ?
    """
    added_errors = np.zeros(3*(dim+1)*dim**2)
    obj_AB = 0
    for j in range(3*(dim+1)*dim**2):
        deltadelta = alpha*candidate_meas[j]
        if j >= 3*dim**2:
            deltadelta -= alpha*candidate_meas[j-3*dim**2]
        if cos(alpha*candidate_errors[j] - deltadelta) < 0:
            added_errors[j] = 1
            obj_AB += 1/(alpha**2*sigma_list[j]**2)*( \
                     - cos(alpha*candidate_errors[j] - deltadelta)*(1 - B[j]*B[j]/8) \
                     + sin(alpha*candidate_errors[j] - deltadelta)*B[j]/2)
        else:
            obj_AB += 1/(alpha**2*sigma_list[j]**2)*( \
                     + cos(alpha*candidate_errors[j] - deltadelta)*(1 - B[j]*B[j]/8) \
                     - sin(alpha*candidate_errors[j] - deltadelta)*B[j]/2)
        obj_AB += 1/(4*alpha**2*sigma_meas**2)*(1 - A[j]*A[j]/2)
    return obj_AB, added_errors


def create_gurobi_model(name, candidate_errors, candidate_meas, sigma_list, sigma_meas, dim, alpha):
    model = grb.Model(name)
    Aaux = model.addVars(3*(dim+1)*dim**2, vtype=grb.GRB.CONTINUOUS, name="Aux") 
    Baux = model.addVars(3*(dim+1)*dim**2, vtype=grb.GRB.CONTINUOUS, name="Baux") 
    rotAaux = rotational(Aaux, dim)
    # divBaux = divergence(Baux, dim)
    model.addConstrs((Aaux[i] == 0 for i in [edge_to_int((a[0], a[1], 0), (a[0]+1, a[1], 0), dim) for a in product(range(dim),range(dim))]), name='Ax_0')
    model.addConstrs((Aaux[i] == 0 for i in [edge_to_int((a[0], a[1], 0), (a[0], a[1]+1, 0), dim) for a in product(range(dim),range(dim))]), name='Ay_0')
    # model.addConstrs((Aaux[i] == 0 for i in [edge_to_int((a[0], a[1], a[2]), (a[0], a[1], a[2]+1), dim) for a in product(range(dim),range(dim),range(dim+1))]), name='Az')
    model.addConstrs((Baux[i] == 0 for i in [edge_to_int((a[0], a[1], dim), (a[0], a[1], dim+1), dim) for a in product(range(dim),range(dim))]), name='last_toric_meas')
    # div_constr = model.addConstrs((divBaux[i] == 0 for i in range((dim+1)*dim**2)), name='div_constr')
    AB_constr = model.addConstrs((rotAaux[i] == Baux[i] for i in range(3*(dim+1)*dim**2)), name="B_rotA_constr")
    obj_AB, added_errors = objective_AB(Aaux, Baux, candidate_errors, candidate_meas, sigma_list, sigma_meas, dim, alpha)
    model.setObjective(obj_AB, grb.GRB.MAXIMIZE)
    model.update()
    model.optimize()
    return model, Aaux, Baux, added_errors


def create_mip_gurobi_model(name, candidate_errors, sigma_list, dim, alpha):
    model = grb.Model(name)
    A = model.addVars(3*(dim+1)*dim**2, lb=-1, ub=1, vtype=grb.GRB.CONTINUOUS, name="A") 
    model.addConstrs((A[i] == 0 for i in [edge_to_int((a[0], a[1], 0), (a[0]+1, a[1], 0), dim) for a in product(range(dim),range(dim))]), name='Ax_0')
    model.addConstrs((A[i] == 0 for i in [edge_to_int((a[0], a[1], 0), (a[0], a[1]+1, 0), dim) for a in product(range(dim),range(dim))]), name='Ay_0')
    for x in range(dim):
        for y in range(dim):
            for z in range(dim+1):
                A[edge_to_int((x, y, z), (x, y, z+1), dim)].setAttr(grb.GRB.Attr.VType, grb.GRB.INTEGER)
    rotA = rotational(A, dim)
    # div = divergence(B, dim)
    # div_constr = model.addConstrs((div[i] == 0 for i in range((dim+1)*dim**2)), name='div_cons')
    obj_rotA, added_errors = objective_rotA(rotA, candidate_errors, sigma_list, dim, alpha)
    model.setObjective(obj_rotA, grb.GRB.MAXIMIZE)
    model.update()
    model.optimize()
    return model, A, added_errors


def decode_via_quadratic_approx(sigma, sigma_toric, sigma_meas, dim, alpha):
    """
    """
    sigma_list = get_sigma_list(sigma, sigma_toric, dim)
    data_errors, meas_errors, cumul_data_errors = sample_error(sigma_list, sigma_meas, dim)
    syndrome_toric, syndrome_gkp = measure_syndrome(data_errors, meas_errors, cumul_data_errors, dim, alpha)
    candidate_data, candidate_meas, candidate_cumul_data = error_candidate(syndrome_toric, syndrome_gkp, dim, alpha)
    model, B, div_constr, added_errors = create_gurobi_model("quadratic_B_opt", candidate_data, sigma_list, dim, alpha)
    return (model, B, added_errors,
            data_errors, meas_errors, cumul_data_errors,
            candidate_data, candidate_meas, candidate_cumul_data,
            syndrome_toric, syndrome_gkp)
