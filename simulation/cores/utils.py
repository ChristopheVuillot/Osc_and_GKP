"""simulation_utils
Some useful function common to all simulations.
"""
from pathlib import Path
import numpy as np
from scipy.optimize import least_squares


def measurement(displacement, alpha):
    """ measurement(displacement, alpha):
    returns the measurement outcome and if there was an error
    from the actual displacement that occured.
    The returned measurement value lies in [-pi, pi[.
    """
    # For the scale we consider the case where
    # 2*alpha*displacement = meas + k*2*pi
    # So there is an error if k is odd.
    # We also translate such that meas \in [-pi;pi[.
    meas = ((2*alpha*displacement + np.pi) % (2*np.pi)) - np.pi
    err = (np.floor((alpha*displacement + np.pi/2)/np.pi) % 2) == 1
    return meas, (-1)**err


def measurement_toric(displacement, alpha):
    """ measurement(displacement, alpha):
    returns the measurement outcome and if there was an error
    from the actual displacement that occured.
    The returned measurement value lies in [-2pi, 2pi[.
    """
    # For the scale we consider the case where
    # 2*alpha*displacement = meas + k*4*pi
    # So there is an error if k is odd.
    # We also translate such that meas \in [-2pi;2pi[.
    return ((2*alpha*displacement + 2*np.pi) % (4*np.pi)) - 2*np.pi


def writing_one_result(prefix_str, var_names, index_name, index_value, var_values):
    """writing_one_result
    write the result of one expe to the correct file
    """
    f_path = Path(prefix_str + index_name + str(index_value) + '.txt')
    exi = f_path.exists()
    with open(prefix_str+index_name+str(index_value)+'.txt', 'a') as res_file:
        if not exi:
            res_file.write(var_names + '\n')
        format_str = ' '.join(['{}' for _ in var_values]) + '\n'
        res_file.write(format_str.format(*var_values))


def merge_error_rates_result_files(filename_list, filename_merged, x_prec=5):
    """merge_error_rates_result_files(filename_list, filename_merged):
    merge several result files in one single file.
    """
    res_dict = {}
    for filename in filename_list:
        with open(filename, 'r') as res_file:
            var_name = res_file.readline()
            for data_point in res_file.readlines():
                if len(data_point.strip()) == 0:
                    continue
                x_value_str, rate_str, trials_str = data_point.split()
                x_value = int(float(x_value_str)*10**x_prec)/10**x_prec
                rate = float(rate_str)
                trials = int(trials_str)
                if x_value not in res_dict:
                    res_dict[x_value] = (rate, trials)
                else:
                    prev_rate, prev_trials = res_dict[x_value]
                    res_dict[x_value] = ((rate*trials+prev_rate*prev_trials)/(trials+prev_trials),
                                         trials+prev_trials)
    with open(filename_merged, 'w') as merged_file:
        merged_file.write(var_name)
        for x_value, (rate, trials) in res_dict.items():
            merged_file.write('{} {} {}\n'.format(x_value, rate, trials))


def merge_error_counts_result_files(filename_list, filename_merged, x_prec=5):
    """
    merge several result files of counts in one single file.
    """
    res_dict = {}
    for filename in filename_list:
        with open(filename, 'r') as res_file:
            var_name = res_file.readline()
            for data_point in res_file.readlines():
                if len(data_point.strip()) == 0:
                    continue
                x_value_str, count_str, trials_str = data_point.split()
                x_value = int(float(x_value_str)*10**x_prec)/10**x_prec
                count = int(count_str)
                trials = int(trials_str)
                if x_value not in res_dict:
                    res_dict[x_value] = (count, trials)
                else:
                    prev_count, prev_trials = res_dict[x_value]
                    res_dict[x_value] = (count+prev_count,
                                         trials+prev_trials)
    with open(filename_merged, 'w') as merged_file:
        merged_file.write(var_name)
        for x_value, (count, trials) in res_dict.items():
            merged_file.write('{} {} {}\n'.format(x_value, count, trials))


def func(param, k):
    return (1 - param[1]*(1 - 2*param[0])**k)/2


def error_func(param, k, y):
    return (1 - param[1]*(1 - 2*param[0])**k)/2 - y


def jac_error_func(param, k, y):
    return np.transpose(np.array([-k*param[1]*(1 - 2*param[0])**(k-1),
                     (1 - 2*param[0])**k/2]))


def fitting_exp(filename_input, filename_output):
    """
    """
    x = []
    y = []
    trials = []
    with open(filename_input, 'r') as file_in:
        var_name = file_in.readline()
        for data_point in file_in.readlines():
            x_value_str, rate_str, trials_str = data_point.split()
            x.append(float(x_value_str))
            y.append(float(rate_str))
            trials.append(int(trials_str))
    param0 = np.array([.1, 1.])
    # fitted_param = least_squares(error_func, param0, jac=jac_error_func, bounds=(np.array([0., 0.]), np.array([.5, 1.])), args=(np.array(x), np.array(y)))
    fitted_param = least_squares(error_func, param0, bounds=(np.array([0., 0.]), np.array([.5, 1.])), args=(np.array(x), np.array(y)))
    # print(fitted_param)
    with open(filename_output, 'w') as file_out:
        file_out.write(var_name)
        for x_value in x:
            file_out.write('{} {} {}\n'.format(x_value, func(fitted_param.x, x_value), 0))
    return fitted_param.x[0]


def print_array(array, index_print):
    return '\n'.join(['{}:{}'.format(index_print(j), v) for j, v in enumerate(array)])
