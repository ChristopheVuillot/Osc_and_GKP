"""space_time_3d_script
...
"""
from itertools import product
import sys
import re
import os
import numpy as np

from simulation.cores.parallel_workers import main_process
from simulation.cores.utils import writing_one_result, merge_error_rates_result_files
from simulation.cores.space_time_3d import one_expe_direct_impl, one_expe_minE_impl, one_expe_no_monotoring_impl, one_expe_optimize
from simulation import MACHINE_ID

ALPHA = np.sqrt(np.pi)
DIM_LIST = [3 + 2 * i for i in range(1)]
SIG_LIST = [(260 + i*10)/1000 for i in range(11)]
N_TRIALS = 1*10**2
ARG_LIST_NO_MEAS_ERR = product(DIM_LIST, SIG_LIST, [N_TRIALS for _ in range(60)], [ALPHA])
# same sigma for data and measurments
FRAC = 1
FRAC_STR = ''
ARG_LIST = [(a, b, FRAC*b, c, d) for (a, b, c, d) in ARG_LIST_NO_MEAS_ERR]

#This prefix has to be unique among the other files present in the folder
FOLDER = 'results/space_time_3d/'
PREFIX_STR = MACHINE_ID + '_test_pre_no_monitoring_manhattan' + FRAC_STR
PREFIX_STR_DIR = MACHINE_ID + '_space_time_direct' + FRAC_STR
PREFIX_STR_OPT = MACHINE_ID + '_space_time_full_opt' + FRAC_STR
PREFIX_STR_NO_MONITORING = MACHINE_ID + '_space_time_no_monitoring' + FRAC_STR
VAR_NAMES = 'sigma logical_error_rate trials'
INDEX_NAME = '_dim_'
MAX_CPU = os.cpu_count()-1
# MAX_CPU = 1

def writing_func_opt(dim, sigma, error_rate, sumvar):
    """writing_func
    including a prefix for the name of the files.
    """
    writing_one_result(FOLDER + PREFIX_STR_OPT,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_rate, sumvar))


def writing_func_no_monitoring(dim, sigma, error_rate, sumvar):
    """writing_func
    including a prefix for the name of the files.
    """
    writing_one_result(FOLDER + PREFIX_STR_NO_MONITORING,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_rate, sumvar))


def writing_func_dir(dim, sigma, error_rate, sumvar):
    """writing_func
    including a prefix for the name of the files.
    """
    writing_one_result(FOLDER + PREFIX_STR_DIR,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_rate, sumvar))


if __name__ == '__main__':

    try:
        print('generating files of the type: ' + PREFIX_STR_NO_MONITORING)
        main_process(MAX_CPU, one_expe_no_monotoring_impl, ARG_LIST, writing_func_no_monitoring)

    except KeyboardInterrupt:
        print('Merging data points...')

        RULE = re.compile(PREFIX_STR_NO_MONITORING + r'\S*')
        FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

        for f in FILE_LIST:
            merge_error_rates_result_files([FOLDER + f], FOLDER + f)

        print('All merged !')
        sys.exit('Interupted before all process done')

    print('Merging data points...')

    RULE = re.compile(PREFIX_STR_NO_MONITORING + r'\S*')
    FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

    for f in FILE_LIST:
        merge_error_rates_result_files([FOLDER + f], FOLDER + f)

    print('All merged !')
