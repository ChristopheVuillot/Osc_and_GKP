"""dual_space_time_3d_script
...
"""
from itertools import product
import re
import os
import sys
import numpy as np

from simulation.cores.parallel_workers import main_process
from simulation.cores.utils import writing_one_result, merge_error_counts_result_files
from simulation.cores.dual_space_time_3d import batch_of_trials, decode_direct, decode_no_gkp_err, decode_no_gkp_err_direct, get_sigma_list, decode_min_gkp
from simulation import MACHINE_ID

ALPHA = np.sqrt(np.pi)
# standard deviation of the input shift error
SIG_LIST = np.linspace(.2, .24, 9)
DIM_LIST = range(3, 10, 2)

N_TRIALS = 1*10**2
ARG_LIST_INIT = product(DIM_LIST, SIG_LIST, [ALPHA], [N_TRIALS for _ in range(20)])
# same sigma for data and toric code error
# sigma/FRAC for sigma_meas
FRAC = 1
FRAC_STR = ''
ARG_LIST = [(get_sigma_list(b,b,a), FRAC*b, a, c, d) for (a, b, c, d) in ARG_LIST_INIT]

#This prefix has to be unique among the other files present in the folder
FOLDER = 'results/dual_space_time_3d/'
PREFIX_STR_DIRECT = MACHINE_ID + '_full3d_direct' + FRAC_STR
PREFIX_STR_PRE_GKP = MACHINE_ID + '_full3d_reyes_bwd_pre_gkp' + FRAC_STR
PREFIX_STR_NO_GKP_ERR = MACHINE_ID + '_full3d_no_gkp_err' + FRAC_STR
VAR_NAMES = 'sigma logical_error_count trials'
INDEX_NAME = '_dim_'

MAX_CPU = os.cpu_count() - 1
# MAX_CPU = 1

def writing_func_direct(sigma, sigma_toric, sigma_meas, dim, error_count, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_DIRECT,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_count, n_trials))


def writing_func_pre_gkp(sigma, sigma_toric, sigma_meas, dim, error_count, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_PRE_GKP,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_count, n_trials))


def writing_func_no_gkp_err(sigma, sigma_toric, sigma_meas, dim, error_count, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_NO_GKP_ERR,
                       VAR_NAMES,
                       INDEX_NAME,
                       dim,
                       (sigma, error_count, n_trials))


def one_expe_direct(sigma_list, sigma_meas, dim, alpha, n_trials):
    return batch_of_trials(sigma_list, sigma_meas, dim, alpha, n_trials, decode_direct)


def one_expe_pre_gkp(sigma_list, sigma_meas, dim, alpha, n_trials):
    return batch_of_trials(sigma_list, sigma_meas, dim, alpha, n_trials, decode_min_gkp)


def one_expe_no_gkp_err(sigma_list, sigma_meas, dim, alpha, n_trials):
    return batch_of_trials(sigma_list, sigma_meas, dim, alpha, n_trials, decode_no_gkp_err)


if __name__ == '__main__':

    try:
        print('generating files of the type: ' + PREFIX_STR_PRE_GKP)
        main_process(MAX_CPU, one_expe_pre_gkp, ARG_LIST, writing_func_pre_gkp)

    except KeyboardInterrupt:
        print('Merging data points...')

        RULE = re.compile(PREFIX_STR_PRE_GKP + r'\S*')
        FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

        for f in FILE_LIST:
            merge_error_counts_result_files([FOLDER + f], FOLDER + f)

        print('All merged !')
        sys.exit('Interupted before all process done')

    print('Merging data points...')

    RULE = re.compile(PREFIX_STR_PRE_GKP + r'\S*')
    FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

    for f in FILE_LIST:
        merge_error_counts_result_files([FOLDER + f], FOLDER + f)

    print('All merged !')
