"""one_gkp_script
...
"""
from itertools import product
import re
import os
import numpy as np

from simulation.cores.parallel_workers import main_process
from simulation.cores.utils import writing_one_result, merge_error_rates_result_files
from simulation.cores.one_gkp import one_expe_one_gkp_no_memory, one_expe_one_gkp_no_meas_error, one_expe_one_gkp_mldecoder, one_expe_one_gkp_minE, one_expe_one_gkp_no_monitoring, iterator_integers
from simulation import MACHINE_ID

ALPHA = np.sqrt(np.pi)
SIG_LIST = [(200 + i*10)/1000 for i in range(1)]  # standard deviation of the input shift error
MAX_STEP = 6
N_STEPS_LIST = range(6, MAX_STEP+1)

N_TRIALS = 1*10**1
ARG_LIST_NO_MEAS_ERR = product(SIG_LIST, N_STEPS_LIST, [ALPHA], [N_TRIALS for _ in range(35)])
# same sigma for data and measurments
FRAC = 1
FRAC_STR = ''
ARG_LIST = [(a, FRAC*a, b, c, d) for (a, b, c, d) in ARG_LIST_NO_MEAS_ERR]
ARG_LIST_MLD2 = product(SIG_LIST, N_STEPS_LIST, [N_TRIALS for _ in range(30)])
K = 2
ARG_LIST_MLD = [(s, FRAC*s, ns, nt, ALPHA, K) for (s, ns, nt) in ARG_LIST_MLD2]
# COMMON_ARGS_MLD = [ALPHA, list_integers(MAX_STEP, K), K]

#This prefix has to be unique among the other files present in the folder
FOLDER = 'results/one_gkp/'
PREFIX_STR = MACHINE_ID + '_GKP_results_test' + FRAC_STR
PREFIX_STR_NO_MEAS_ERR = MACHINE_ID + '_GKP_results_no_meas_err' + FRAC_STR
PREFIX_STR_NO_MONITORING = MACHINE_ID + '_GKP_no_monitoring' + FRAC_STR
PREFIX_STR_MLD = MACHINE_ID + '_GKP_new2approx_MLD' + FRAC_STR
PREFIX_STR_MINE = MACHINE_ID + '_GKP_minE' + FRAC_STR
VAR_NAMES = 'n_steps logical_error_rate trials'
INDEX_NAME = '_sigma3digit_'

MAX_CPU = os.cpu_count() - 1
# MAX_CPU = 1

def writing_func(sigma, _, n_steps, error_rate, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR,
                       VAR_NAMES,
                       INDEX_NAME,
                       int(sigma*1000),
                       (n_steps, error_rate, n_trials))


def writing_func_no_meas_err(sigma, n_steps, error_rate, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_NO_MEAS_ERR,
                       VAR_NAMES,
                       INDEX_NAME,
                       int(sigma*1000),
                       (n_steps, error_rate, n_trials))


def writing_func_no_monitoring(sigma, n_steps, error_rate, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_NO_MONITORING,
                       VAR_NAMES,
                       INDEX_NAME,
                       int(sigma*1000),
                       (n_steps, error_rate, n_trials))


def writing_func_MLD(sigma, _, n_steps, error_rate, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_MLD,
                       VAR_NAMES,
                       INDEX_NAME,
                       int(sigma*1000),
                       (n_steps, error_rate, n_trials))

def writing_func_minE(sigma, _, n_steps, error_rate, n_trials):
    """writing_func
    ...
    """
    writing_one_result(FOLDER + PREFIX_STR_MINE,
                       VAR_NAMES,
                       INDEX_NAME,
                       int(sigma*1000),
                       (n_steps, error_rate, n_trials))


if __name__ == '__main__':

    print('generating files of the type: ' + PREFIX_STR_MLD)
    main_process(MAX_CPU, one_expe_one_gkp_mldecoder, ARG_LIST_MLD, writing_func_MLD)

    print('Merging data points...')

    RULE = re.compile(PREFIX_STR_MLD + r'\S*')
    FILE_LIST = [f for f in os.listdir(FOLDER) if RULE.match(f)]

    for f in FILE_LIST:
        merge_error_rates_result_files([FOLDER + f], FOLDER + f)

    print('All merged !')
