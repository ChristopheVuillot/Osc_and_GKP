"""merge_only.py
"""

from sys import argv
from getopt import getopt, GetoptError
import re
import os

from simulation.cores.utils import merge_error_rates_result_files, merge_error_counts_result_files


if __name__ == "__main__":
    try:
        OPTS, ARGS = getopt(argv[1:], "m:f:n:e:c:")
    except GetoptError:
        print('Usage: merge_only.py -m id1,id2,id3 -f filebasename -n mergefileid -e expefoldername -c countorrate')
        exit(2)

    NARG = [0,0,0,0,0]
    for opt, arg in OPTS:
        if opt == '-m':
            MACHINE_LIST = arg.split(',')
            NARG[0] = 1
        elif opt == '-f':
            FILE = arg
            NARG[1] = 1
        elif opt == '-n':
            MERGE_NAME = arg
            NARG[2] = 1
        elif opt == '-e':
            FOLDER = 'results/' + arg + '/'
            NARG[3] = 1
        elif opt =='-c':
            if arg == 'counts':
                COUNT = True
            elif arg == 'rates':
                COUNT = False
            else:
                print('after -c put only \'counts\' or \'rates\' to indicate when merging count files or rate files.')
                exit(2)
            NARG[4] = 1
        else:
            print('Usage: merge_only.py -m id1,id2,id3 -f filebasename -n mergefileid -e expefoldername -c countorrate')
            exit(2)
    if sum(NARG) < 5:
        print('Not enough arguments\nUsage: merge_results_script.py -m id1,id2,id3 -f filebasename -n mergefileid -e expefoldername -c countorrate')
        exit(2)

    print('Merging machines :', ', '.join(MACHINE_LIST))
    print('Base name of the type', FOLDER + MACHINE_LIST[0] + '_' + FILE)
    print('Creating files of the type', FOLDER + '_'.join([MERGE_NAME, FILE]))

    RULES = []
    for MACHINE_NAME in MACHINE_LIST:
        RULES.append(re.compile('_'.join([MACHINE_NAME, FILE]) + r'(\S*)'))

    FILE_DICT = {}
    for f in os.listdir(FOLDER):
        matches = [rule.match(f) for rule in RULES]
        for m in matches:
            if m:
                FILE_DICT.setdefault(m.group(1), []).append(FOLDER + f)

    number = 0
    for sig, files in FILE_DICT.items():
        if COUNT:
            merge_error_counts_result_files(files, FOLDER + '_'.join([MERGE_NAME, FILE]) + sig)
        else:
            merge_error_rates_result_files(files, FOLDER + '_'.join([MERGE_NAME, FILE]) + sig)
        number += 1

    print('{} equivalent file processed !'.format(number))
